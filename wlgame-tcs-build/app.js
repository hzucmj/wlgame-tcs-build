var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require("express-session");
var favicon = require('serve-favicon')

var indexRouter = require('./routes/login/login');
var adminRouter = require('./routes/admin');
var userRouter = require('./routes/user/userIndex');
var roleRouter = require('./routes/user/roleIndex');
var dashboardRouter = require('./routes/dashboard');
var buildRouter = require('./routes/build/index');
var menuRouter = require('./routes/menu/index');
var branchRouter = require('./routes/branch/index');
var versionRouter = require('./routes/version/index');
var permissionRouter = require('./routes/permission/index');
var taskRouter = require('./routes/task/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

var db = require("./server/libs/db");
db.init();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
	secret: 'q@e$t^u*',
	name: "wlgame-build-sys",
	cookie: {maxAge: 12 * 3600 * 1000}, // 12小时后过期
	resave: false,
	saveUninitialized: true
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(path.join(__dirname, "public", "favicon.ico")));

app.use('/', adminRouter);
app.use('/user', userRouter);
app.use('/role', roleRouter);
app.use('/dashboard', dashboardRouter);
app.use('/index', indexRouter);
app.use('/menu', menuRouter);
app.use('/build', buildRouter);
app.use('/permission', permissionRouter);
app.use('/task', taskRouter);
app.use('/branch', branchRouter);
app.use("/version", versionRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

var express = require('express');
var router = express.Router();

var db = require("../../server/libs/db");

/* GET home page. */
router.get('/', function (req, res, next) {
	let page = req.query.page || 1;
	let pageSize = 10;
	let startIndex = (page - 1) * pageSize;
    var data = {
        module: "分支管理", 
        title: '分支列表',
        branches: []
    };
	let sql = "SELECT COUNT(*) totalCount FROM tb_branch";
	db.executeSql(sql, function (err, ds, fields) {
		if (err) {
			res.render('branch/index', data);
			return;
		}

		if (ds.length == 0) {
			res.render('branch/index', data);
			return;
		}

		let totalCount = ds[0].totalCount;
		let totalPage = Math.ceil(totalCount / pageSize);
		sql = "SELECT * FROM tb_branch ORDER BY createtime DESC LIMIT " + startIndex + ", " + pageSize + ";";
		db.executeSql(sql, function (err, ds, fields) {
			if (err) {
				res.render("branch/index", data);
				return ;
			}

			var branches = [];
			for (var i = 0; i < ds.length; i++) {
				var b = {};
				b.id = ds[i].id;
				b.order = i + 1;
				if (ds[i].repostitory == 0) {
					b.repostitory = "game-hall-client-new";
				} else if (ds[i].repostitory == 1) {
					b.repostitory = "client-lobby";
				} else if (ds[i].repostitory == 2) {
					b.repostitory = "MahjongUI";
				}
				// b.repostitory = ds[i].repostitory;
				b.branch = ds[i].branch;
				b.description = ds[i].description;
				b.giturl = ds[i].giturl;
				b.type = ds[i].type;
				branches.push(b);
			}
			data.branches = branches;
			data.lastPage = parseInt(page) - 1;
			data.currentPage = parseInt(page);
			data.nextPage = parseInt(page) + 1;
			data.maxPage = totalPage;
			data.navUrl = "/branch/";
			res.render("branch/index", data);
		});
	});
});

router.get("/saveBranch", function (req, res, next) {
	let repostitory = req.query.repostitory;
	let description = req.query.description;
	let branch = req.query.branch;
	let isUpdate = req.query.isUpdate;
	let type = repostitory;
	let id = req.query.id;

	let data = {
		repostitory: repostitory,
		description: description,
		branch: branch,
		type: type
	};
	if (isUpdate) {
		let indexData = {
			id: id
		};
		db.updateTableRecord("tb_branch", indexData, data, function (err, ds, fields) {
			if (err) {
				console.log(err);
				res.send(JSON.stringify({success: false, msg: "系统错误"}));
				return ;
			}

			res.send(JSON.stringify({success: true, msg: "保存成功"}));
		});
	} else {
		let sql = "SELECT * FROM tb_branch t WHERE t.branch = '" + branch + "' AND t.repostitory = '" + repostitory + "'";
		db.executeSql(sql, function (err, ds, fields) {
			if (err) {
				res.send(JSON.stringify({success: false, msg: "系统错误"}));
				return ;
			}

			if (ds.length > 0) {
				res.send(JSON.stringify({success: false, msg: "已存在相同的分支"}));
				return ;
			}

			db.insertTableRecord("tb_branch", data, function (err, ds, fields) {
				if (err) {
					res.send(JSON.stringify({success: false, msg: "系统错误"}));
					return ;
				}

				res.send(JSON.stringify({success: true, msg: "保存成功"}));
			});
		});
	}
});

router.get("/delBranch", function (req, res, next) {
	let id = req.query.id;
	if (!id || id.length <= 0) {
		res.send(JSON.stringify({success: false, msg: "参数id不能为空"}));
		return ;
	}

	let sql = "DELETE FROM tb_branch WHERE id = " + id;
	db.executeSql(sql, function (err, ds, fields) {
		if (err) {
			console.log(err);
			res.send(JSON.stringify({success: false, msg: "删除记录失败"}));
			return ;
		}

		res.send(JSON.stringify({success: true, msg: "删除记录成功"}));
	});
});

router.get("/getBranch", function (req, res, next) {
	let id = req.query.id;
	if (!id || id.length === 0) {
		res.send(JSON.stringify({success: false, msg: "参数id不能为空"}));
		return ;
	}

	let sql = "SELECT * FROM tb_branch t WHERE t.id = '" + id + "'";
	db.executeSql(sql, function (err, ds, fields) {
		if (err) {
			res.send(JSON.stringify({success: false, msg: "查询记录失败"}));
			return;
		}

		if (ds.length === 0) {
			res.send(JSON.stringify({success: false, msg: "记录不存在"}));
			return ;
		}

		let data = {
			id: ds[0].id,
			branch: ds[0].branch,
			repostitory: ds[0].repostitory,
			description: ds[0].description,
			type: ds[0].type
		};
		res.send(JSON.stringify({success: true, msg: "", data: data}));
	});
});

router.get("/getAllBranch", function (req, res, next) {
	let sql = "SELECT repostitory, branch FROM tb_branch GROUP BY repostitory, branch;";
	db.executeSql(sql, function (err, ds, fields) {
		if (err) {
			console.log(err);
			res.send(JSON.stringify({success: false, msg: "查询记录失败"}));
			return ;
		}

		let data = {
			gameHallBranch: [],
			clientLobbyBranch: [],
			mahjongUIBranch: []
		};
		for (let idx in ds) {
			if (ds[idx].repostitory === 0) {
				data.gameHallBranch.push(ds[idx]);
			} else if (ds[idx].repostitory === 1) {
				data.clientLobbyBranch.push(ds[idx]);
			} else if (ds[idx].repostitory === 2) {
				data.mahjongUIBranch.push(ds[idx]);
			}
		}

		res.send(JSON.stringify({success: true, msg: "加载分支成功", data: data}));
	})
});

module.exports = router;

var express = require('express');
var router = express.Router();
var db = require("../../server/libs/db");

router.get('/', function (req, res, next) {

	let page = req.query.page || 1;
	let pageSize = 10;
	let startIndex = (page - 1) * pageSize;

    let data = {
        module: "概况",
        title: '打包队列',
        allTasks: []
    };

	let sql = "SELECT COUNT(*) totalCount FROM tb_task t WHERE t.status <= 1";
	db.executeSql(sql, function (err, ds, fields) {
		if (err) {
			res.render('task/index', data);
			return;
		}

		if (ds.length == 0) {
			res.render('task/index', data);
			return;
		}

		let totalCount = ds[0].totalCount;
		let totalPage = Math.ceil(totalCount / pageSize);
		sql = "SELECT * FROM tb_task t WHERE t.status <= 1 ORDER BY createtime DESC LIMIT " + startIndex + "," + pageSize + ";";
		db.executeSql(sql, function (err, ds, fields) {
			if (err) {
				res.render('task/index', data);
				return;
			}
	
			let allTasks = [];
			for (let i = 0; i < ds.length; i++) {
				let task = {};
				task.order = i + 1;
				task.taskid = ds[i].taskid;
				task.params = ds[i].params;
	
				let type = parseInt(ds[i].type);
				let typeText = "";
				if (type === 0) {
					typeText = "Android包";
				} else if (type === 1) {
					typeText = "IOS包";
				} else if (type === 2) {
					typeText = "Android&IOS包";
				} else if (type === 3) {
					typeText = "大厅ZIP包";
				} else if (type === 4) {
					typeText = "子游戏ZIP包"
				}
                task.type = type;
				task.typeText = typeText;
	
				let statusText = "未开始";
				let status = parseInt(ds[i].status);
				if (status === 0) {
					statusText = "未开始";
				} else if (status === 1) {
					statusText = "构建中";
				} else if (status === 2) {
					statusText = "构建完成";
				} else if (status === 3) {
					statusText = "构建失败";
				} else if (status === 4) {
					statusText = "构建失败，参数不正确";
				} else if (status === 5) {
					statusText = "构建完成，但上传FTP不成功";
				} else if (status === 6) {
					statusText = "构建完成，但上传FIR不成功";
				} else if (status === 7) {
					statusText = "构建完成，但发送邮件不成功";
				} else {
					statusText = "未知状态";
				}
				task.statusText = statusText;
				task.status = ds[i].status;
				task.apkurl = ds[i].apkurl;
				task.iosurl = ds[i].iosurl;
				task.owner = ds[i].owner;
				task.description = ds[i].description;
				var date = new Date(ds[i].createtime);
				var dateFormat = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
				task.createtime = dateFormat;
				allTasks.push(task);
			}
			data.allTasks = allTasks;
			data.lastPage = parseInt(page) - 1;
			data.currentPage = parseInt(page);
			data.nextPage = parseInt(page) + 1;
			data.maxPage = totalPage;
			data.navUrl = "/task/";
			res.render('task/index', data);
		});
	});
	
});

router.get('/history', function (req, res, next) {

	let page = req.query.page || 1;
	let pageSize = 10;
	let startIndex = (page - 1) * pageSize;

	let data = {
		module: "打包记录",
		title: '',
		allTasks: []
	};
	let sql = "SELECT COUNT(*) totalCount FROM tb_task t WHERE t.status > 1";
	db.executeSql(sql, function (err, ds, fields) {
		if (err) {
			res.render('task/index', data);
			return;
		}

		if (ds.length == 0) {
			res.render('task/index', data);
			return;
		}

		let totalCount = ds[0].totalCount;
		let totalPage = Math.ceil(totalCount / pageSize);
		sql = "SELECT * FROM tb_task t WHERE t.status > 1 ORDER BY createtime DESC LIMIT " + startIndex + "," + pageSize + ";";
		db.executeSql(sql, function (err, ds, fields) {
			if (err) {
				res.render('task/index', data);
				return;
			}

			let allTasks = [];
			for (let i = 0; i < ds.length; i++) {
				let task = {};
				task.order = i + 1;
				task.taskid = ds[i].taskid;
				task.params = ds[i].params;
				task.type = ds[i].type;

				let type = parseInt(ds[i].type);
				let typeText = "底包";
				if (type === 0) {
					typeText = "Android包";
				} else if (type === 1) {
					typeText = "IOS包";
				} else if (type === 2) {
					typeText = "Android&IOS包";
				} else if (type === 3) {
					typeText = "大厅ZIP包";
				} else if (type === 4) {
					typeText = "子游戏ZIP包"
				}
                task.type = type;
				task.typeText = typeText;

				let statusText = "未开始";
				let status = parseInt(ds[i].status);
				if (status === 0) {
					statusText = "未开始";
				} else if (status === 1) {
					statusText = "构建中";
				} else if (status === 2) {
					statusText = "构建完成";
				} else if (status === 3) {
					statusText = "构建失败";
				} else if (status === 4) {
					statusText = "构建失败，参数不正确";
				} else if (status === 5) {
					statusText = "构建完成，但上传FTP不成功";
				} else if (status === 6) {
					statusText = "构建完成，但上传FIR不成功";
				} else if (status === 7) {
					statusText = "构建完成，但发送邮件不成功";
				} else {
					statusText = "未知状态";
				}
				task.statusText = statusText;
				task.status = ds[i].status;
				task.apkurl = ds[i].apkurl;
				task.iosurl = ds[i].iosurl;
                task.owner = ds[i].owner;
                task.description = ds[i].description;
				var date = new Date(ds[i].createtime);
				var dateFormat = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
				task.createtime = dateFormat;
				allTasks.push(task);
			}
			data.allTasks = allTasks;
			data.lastPage = parseInt(page) - 1;
			data.currentPage = parseInt(page);
			data.nextPage = parseInt(page) + 1;
			data.maxPage = totalPage;
			data.navUrl = "/task/history/";
			res.render('task/history', data);
		});
	});
});

let addTask = function (res, data) {
	db.insertTableRecord("tb_task", data, function (err, ds, fields) {
		if (err) {
			res.send(JSON.stringify({ success: false, msg: "提交任务失败，请稍候重试！" }));
			return;
		}

		res.send(JSON.stringify({ success: true, msg: "提交任务成功，请等待构建完成！" }));
	});
};

// 打底包
// 名叫buildApk, 但是可以打ios包
router.get("/addTask", function (req, res, next) {
	console.log("/addTask", req.query);
	let configId = req.query.configId;
	if (!configId || configId.length === 0) {
		// 没有id，是直接打包
		let type = parseInt(req.query.type);
		if (type === 0) {
			let params = req.query.params;
			if (!params || params.length === 0) {
				res.send(JSON.stringify({ success: false, msg: "构建参数不能为空！" }));
				return;
			}

			let type = req.query.type || -1;
			if (type < 0 || type > 3) {
				res.send(JSON.stringify({ success: false, msg: "构建类型错误！" }));
				return;
			}

			let data = {
				taskid: "T" + (new Date()).getTime(),
				params: params,
				type: type,
				owner: req.session.username
			};
			addTask(res, data);
		} else if (type === 1) {

		} else if (type === 2) {

		}
	} else {
		let sql = "SELECT * FROM tb_build_cfg WHERE id=" + configId;
		db.executeSql(sql, function (err, ds, fields) {
			if (err) {
				res.send(JSON.stringify({success: false, msg: "查询配置失败"}));
				return ;
			}

			if (ds.length === 0) {
				res.send(JSON.stringify({success: false, msg: "打包配置不存在"}));
				return ;
			}

			let cfg = ds[0];
            let type = -1;
			if (cfg.type === 0) {
				let platform = "Android";
				if (req.query.type === "apk") {
                    platform = "Android";
                    type = 0;
				} else if (req.query.type === "ios") {
                    platform = "iOS";
                    type = 1;
				} else if (req.query.type === "all") {
                    platform = "Android&iOS";
                    type = 2;
                } else if (req.query.type == "hallzip") {
                    type = 3;
                } else if (req.query.type == "gamezip") {
                    type = 4;
                }
				let params = JSON.parse(cfg.params);
				params.platform = platform;
				cfg.params = JSON.stringify(params);
			}

			let data = {
				taskid: "T" + (new Date()).getTime(),
				params: cfg.params,
				type: type,
                owner: req.session.username,
                description: cfg.description
			};
			addTask(res, data);
		});
	}
});

module.exports = router;

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('version/index', { module: "版本管理", title: '版本列表' });
});

module.exports = router;

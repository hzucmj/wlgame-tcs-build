let express = require('express');
let router = express.Router();
let db = require("../../server/libs/db");

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('build/index', { module: '版本构建', title: '版本构建' });
});

router.get('/apk', function (req, res, next) {
    res.render('build/build_apk', { module: '版本构建', title: 'APK包' });
});

router.get('/gamezip', function (req, res, next) {
    res.render('build/build_gamezip', { module: '版本构建', title: '子游戏ZIP包' });
});

router.get('/hallzip', function (req, res, next) {
    res.render('build/build_hallzip', { module: '版本构建', title: '大厅ZIP包' });
});

router.get('/allconfigs', function (req, res, next) {
    let keyword = req.query.keyword || "";
	let page = req.query.page || 1;
	let pageSize = 10;
	let startIndex = (page - 1) * pageSize;

	let data = { 
		module: '版本构建', 
		title: '打包配置', 
		config: [] 
	};
    let sql = "SELECT COUNT(*) totalCount FROM tb_build_cfg WHERE enabled = 1";
    if (keyword.length > 0) {
        sql += ` AND (name LIKE '%${keyword}%' OR version LIKE '%${keyword}%' OR description LIKE '%${keyword}%' OR creator LIKE '%${keyword}%')`;
    }
	db.executeSql(sql, function (err, ds, fields) {
		if (err) {
			res.render('build/allconfigs', data);
			return;
		}

		if (ds.length == 0) {
			res.render('build/allconfigs', data);
			return;
		}

		let totalCount = ds[0].totalCount;
		let totalPage = Math.ceil(totalCount / pageSize);
        sql = "SELECT * FROM tb_build_cfg WHERE enabled = 1";
        if (keyword.length > 0) {
            sql += ` AND (name LIKE '%${keyword}%' OR version LIKE '%${keyword}%' OR description LIKE '%${keyword}%' OR creator LIKE '%${keyword}%')`;
        }
        sql +=  " ORDER BY createtime DESC LIMIT " + startIndex + ", " + pageSize + ";"
		db.executeSql(sql, function (err, ds, fields) {
			if (err) {
				console.log(err);
				res.render('build/allconfigs', data);
				return ;
			}

			let configs = [];

			for (let idx = 0; idx < ds.length; idx++) {
				let cfg = {};
				cfg.order = idx + 1;
				cfg.id = ds[idx].id;
				cfg.name = ds[idx].name;
				cfg.type = ds[idx].type;
				if (ds[idx].type === 0) {
					cfg.typeText = "大厅底包";
				} else if (ds[idx] === 1) {
					cfg.typeText = "大厅ZIP包";
				} else {
					cfg.typeText = "子游戏ZIP包";
				}
				cfg.enabled = ds[idx].enabled;
				cfg.description = ds[idx].description;
				cfg.version = ds[idx].version;
				cfg.creator = ds[idx].creator;
				cfg.params = JSON.parse(ds[idx].params);
				configs.push(cfg);
			}
			data.configs = configs;
			data.lastPage = parseInt(page) - 1;
			data.currentPage = parseInt(page);
			data.nextPage = parseInt(page) + 1;
			data.maxPage = totalPage;
			data.navUrl = "/build/allconfigs/";
			res.render('build/allconfigs', data);
		});
	});
});

router.get("/saveConfig", function (req, res, next) {
	let params = {
		platform: req.query.platform,
		mode: req.query.mode,
		gamehallbranch: req.query.gamehallbranch,
		lobbybranch: req.query.lobbybranch,
		env: req.query.env,
		forceclean: req.query.forceclean,
		email: req.query.email,
		subhead: req.query.subhead,
		fir_api_token: req.query.fir_api_token,
		pngquant: req.query.pngquant,
	};
	let data = {
		name: req.query.name,
		version: req.query.version,
		type: req.query.type,
		enabled: req.query.enabled,
		description: req.query.description,
		params: JSON.stringify(params),
		creator: req.session.username
	};
	let isUpdate = req.query.isUpdate;

	if (isUpdate) {
		let indexData = {
			id: req.query.id
		};
		db.updateTableRecord("tb_build_cfg", indexData, data, function (err, ds, fields) {
			if (err) {
				console.log(err);
				res.send(JSON.stringify({ success: false, msg: "保存失败，请稍候重试！" }));
				return;
			}
			res.send(JSON.stringify({ success: true, msg: "保存成功！" }));
		});
	} else {
		let sql = "SELECT * FROM tb_build_cfg WHERE name = '" + req.query.name + "'";
		db.executeSql(sql, function (err, ds, fields) {
			if (err) {
				console.log("/saveConfig", err);
				res.send(JSON.stringify({success: false, msg: "系统错误"}));
				return ;
			}
			if (ds.length > 0) {
				console.log("配置已存在");
				res.send(JSON.stringify({success: false, msg: "配置已存在"}));
				return ;
			}

			db.insertTableRecord("tb_build_cfg", data, function (err, ds, fields) {
				if (err) {
					console.log(err);
					res.send(JSON.stringify({ success: false, msg: "保存失败，请稍候重试！" }));
					return;
				}
				res.send(JSON.stringify({ success: true, msg: "保存成功！" }));
			});
		});
	}
});

router.get("/delConfig", function (req, res, next) {
	let id = req.query.id;
	if (!id || id.length === 0) {
		res.send(JSON.stringify({success: false, msg: "参数id不能为空"}));
		return ;
	}

	let sql = "DELETE FROM tb_build_cfg WHERE id = " + id;
	db.executeSql(sql, function (err, ds, fields) {
		if (err) {
			console.log("/delConfig", err);
			res.send(JSON.stringify({success: false, msg: "系统错误"}));
			return ;
		}

		res.send(JSON.stringify({success: true, msg: "删除配置成功"}));
	});
});

router.get("/getConfig", function (req, res, next) {
	let id = req.query.id;
	if (!id || id.length === 0) {
		res.send(JSON.stringify({success: false, msg: "参数id不能为空"}));
		return ;
	}

	let sql = "SELECT * FROM tb_build_cfg t WHERE t.id='" + id + "' LIMIT 0, 1";
	db.executeSql(sql, function (err, ds, fields) {
		if (err) {
			console.log("/getConfig", err);
			res.send(JSON.stringify({success: false, msg: "系统错误"}));
			return ;
		}
		let data = {};
		if (ds.length <= 0) {
			res.send(JSON.stringify({success: true, msg: "查询成功", data: data}));
			return ;
		}
		ds[0].params = JSON.parse(ds[0].params);
		data = ds[0];
		res.send(JSON.stringify({success: true, msg: "查询成功", data: data}));
	});
});

module.exports = router;

var express = require('express');
var router = express.Router();
var db = require("../../server/libs/db");

router.get("/", function (req, res, next) {
    var data = {
        module: "权限管理", 
        title: '权限设置',
		roleItems: {},
		allMenus: {}
    };

    var sql = "SELECT rid, role FROM tb_role";
    db.executeSql(sql, function (err, ds, fields) {
        if (err) {
        	return;
        }
        data.roleItems = ds;

		let sql = "select t1.mid rootMid, t1.text rootText, t2.mid mid, t2.text text, t2.root root, t2.link link  from (select * from tb_menu where root = 1) t1 left join tb_menu t2 on t1.mid = t2.root or t1.mid = t2.mid";
		db.executeSql(sql, function (err, ds, fields) {
			if (err) {
	            res.render('permission/index', data);
				return;
			}

			if (ds.length === 0) {
	            res.render('permission/index', data);
				return ;
			}
			let allMenus = [];
			for (let idx in ds) {
				let rootMid = ds[idx].rootMid;
				if (!allMenus[rootMid]) {
					allMenus[rootMid] = [];
					let titleMenu = {};
					titleMenu.isTitle = true;
					titleMenu.text = ds[idx].rootText;
					allMenus[rootMid].push(titleMenu);
				}
				let menu = {};
				menu.isTitle = false;
				menu.mid = "menu_" + ds[idx].mid;
				menu.text = ds[idx].text;
				menu.root = ds[idx].root;
				allMenus[rootMid].push(menu);
			}
			console.log(allMenus);
			data.allMenus = allMenus;

			let maxItemCount = 0;
			for (let idx in allMenus) {
				if (allMenus[idx].length > maxItemCount) {
					maxItemCount = allMenus[idx].length;
				}
			}
			data.maxItemCount = maxItemCount;
			res.render("permission/permissionIndex", data);
		});
        
    })

})

router.get("/searchRoleConfig", function (req, res, next) { 
	let sql = "select * from tb_role_permission where rid = " + req.query.index;
	db.executeSql(sql, function (err, ds, fields) {
		 if (err) {
        	return;
        }

        res.send(ds);
	})
})

router.get("/saveRoleConfig", function (req, res, next) {
    let sql = "SELECT * FROM tb_role_permission WHERE rid =" + req.query.updateIndex.rid + ";"
    db.executeSql(sql, function (err, ds, fields) {
    	if (err) {
            res.send(JSON.stringify({ success: false, msg: "保存权限失败，请稍候重试！" }));
            return;
        }

        if (ds.length == 0){
        	req.query.updateData.rid = req.query.updateIndex.rid;
			db.insertTableRecord("tb_role_permission", req.query.updateData, function (err, ds, fields) {
		        if (err) {
		            res.send(JSON.stringify({ success: false, msg: "保存权限失败，请稍候重试！" }));
		            return;
		        }

		        res.send(JSON.stringify({ success: true, msg: "保存权限成功！" }));
		    });
        }else{
		 	db.updateTableRecord("tb_role_permission", req.query.updateIndex, req.query.updateData, function (err, ds, fields) {
		        if (err) {
		            res.send(JSON.stringify({ success: false, msg: "保存权限失败，请稍候重试！" }));
		            return;
		        }

		        res.send(JSON.stringify({ success: true, msg: "保存权限成功！" }));
		    });
        }
    });
})

router.get("/searchUser", function (req, res, next) { 
	let sql = "select * from tb_user where name LIKE '%" + req.query.searchName + "%'";
	db.executeSql(sql, function (err, ds, fields) {
		 if (err) {
        	return;
        }

        var users = [];
        for (var i = 0; i < ds.length; i++) {
            var user = {};
            user.uid = ds[i].uid;
            user.name = ds[i].name;
            users.push(user);
        }

       res.send(users);
	})
})

router.get("/searchUserConfig", function (req, res, next) { 
	let sql = "select * from tb_user_permission where uid = " + req.query.index;
	db.executeSql(sql, function (err, ds, fields) {
		 if (err) {
        	return;
        }

        res.send(ds);
	})
})

router.get("/saveUserConfig", function (req, res, next) {
    let sql = "SELECT * FROM tb_user_permission WHERE uid =" + req.query.updateIndex.uid;
    db.executeSql(sql, function (err, ds, fields) {
    	if (err) {
            res.send(JSON.stringify({ success: false, msg: "保存权限失败，请稍候重试！" }));
            return;
        }

        if (ds.length == 0){
        	req.query.updateData.uid = req.query.updateIndex.uid;
			db.insertTableRecord("tb_user_permission", req.query.updateData, function (err, ds, fields) {
		        if (err) {
		            res.send(JSON.stringify({ success: false, msg: "保存权限失败，请稍候重试！" }));
		            return;
		        }

		        res.send(JSON.stringify({ success: true, msg: "保存权限成功！" }));
		    });
        }else{
		 	db.updateTableRecord("tb_user_permission", req.query.updateIndex, req.query.updateData, function (err, ds, fields) {
		        if (err) {
		            res.send(JSON.stringify({ success: false, msg: "保存权限失败，请稍候重试！" }));
		            return;
		        }

		        res.send(JSON.stringify({ success: true, msg: "保存权限成功！" }));
		    });
        }
    });
})

module.exports = router;

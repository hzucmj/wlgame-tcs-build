var express = require('express');
var router = express.Router();
var db = require("../../server/libs/db");

router.get("/", function (req, res, next) {
    var data = {
        module: "权限管理", 
        title: '权限设置',
    };
	res.render("permission/index", data);
});

let queryPermission = function (sql, res) {
	if (!sql) {
		res.send(JSON.stringify({success: false, msg: "查询失败！"}));
		return;
	}

	db.executeSql(sql, (err, ds, fields) => {
		if (err) {
			res.send(JSON.stringify({success: false, msg: "查询失败！"}));
			return;
		}
		let data = {};
		let allMenus = {};
		for (let idx in ds) {
			let rootMid = ds[idx].rootMid;
			if (!allMenus[rootMid]) {
				allMenus[rootMid] = [];
				let titleMenu = {};
				titleMenu.isTitle = true;
				titleMenu.mid = ds[idx].mid;
				titleMenu.text = ds[idx].rootText;
				titleMenu.root = ds[idx].root;
				titleMenu.link = ds[idx].link;
				titleMenu.enabled = ds[idx].enabled;
				allMenus[rootMid].push(titleMenu);
			} 

			let menu = {};
			menu.isTitle = false;
			menu.mid = ds[idx].mid;
			menu.text = ds[idx].text;
			menu.root = ds[idx].root;
			menu.link = ds[idx].link;
			menu.enabled = ds[idx].enabled;
			allMenus[rootMid].push(menu);
		}
		let maxItemCount = 0;
		for (let idx in allMenus) {
			if (allMenus[idx].length > maxItemCount) {
				maxItemCount = allMenus[idx].length;
			}
		}

		let str = JSON.stringify({success: true, msg: "查询成功！", data: {maxItemCount: maxItemCount, menus: allMenus}}); 
		console.log(str);
		res.send(str);
	});
}

let queryRolePermission = function (req, res, next) {
	// 查询用户组权限
	let rid = req.query.rid;
	if (!rid || rid.length <= 0) {
		res.send(JSON.stringify({success: false, msg: "查询参数错误！"}))
		return ;
	}
	let sql = "select t1.mid rootMid, t1.text rootText, t2.mid mid, t2.text text, t2.root root, t2.link link, if(t3.enabled=1, 1, 0) AS enabled  ";
	sql += "from (select * from tb_menu where root = 1) t1 ";
	sql += "left join tb_menu t2 ";
	sql += "on (t1.mid = t2.root or t1.mid = t2.mid) ";
	sql += "left join tb_role_permission t3 ";
	sql += "on t3.rid = " + rid + " AND t3.menuid = t2.mid ";
	sql += "ORDER BY t2.mid";

	queryPermission(sql, res);
};

let queryUserPermission = function (req, res, next) {
	// 查询用户组权限
	let uid = req.query.uid;
	if (!uid || uid.length <= 0) {
		res.send(JSON.stringify({success: false, msg: "查询参数错误！"}))
		return ;
	}
	let sql = "select t1.mid rootMid, t1.text rootText, t2.mid mid, t2.text text, t2.root root, t2.link link, if(t3.enabled=1, 1, 0) AS enabled  ";
	sql += "from (select * from tb_menu where root = 1) t1 ";
	sql += "left join tb_menu t2 ";
	sql += "on (t1.mid = t2.root or t1.mid = t2.mid) ";
	sql += "left join tb_user_permission t3 ";
	sql += "on t3.uid = " + uid + " AND t3.menuid = t2.mid ";
	sql += "ORDER BY t2.mid";

	queryPermission(sql, res);
};

router.get("/queryPermission", (req, res, next) => {
	let type = req.query.type;
	if (parseInt(type) === 0) {
		queryRolePermission(req, res, next);
	} else if(parseInt(type) === 1) {
		queryUserPermission(req, res, next);
	} else {
		res.send(JSON.stringify({success: false, msg: "查询参数错误！"}));
	}
});

router.get("/savePermission", (req, res, next) => {
	let uid = req.query.uid;
	let rid = req.query.rid;
	let detail = req.query.detail;
	if (uid) {	
		let sql = "DELETE FROM tb_user_permission WHERE uid = " + uid;
		db.executeSql(sql, err => {
			if (err) {
				res.send(JSON.stringify({success: false, msg: "保存失败！"}));
				return ;
			}

			detail.forEach(el => {
				sql = "INSERT INTO tb_user_permission (uid, menuid, enabled) VALUES (" + uid + ", " + el.menuId + ", " + el.enabled + ")";
				db.executeSql(sql, err => {

				});
			});
			res.send(JSON.stringify({success: true, msg: "保存成功！"}));
		});
	} else if (rid) {
		let sql = "DELETE FROM tb_role_permission WHERE rid = " + rid;
		db.executeSql(sql, err => {
			if (err) {
				res.send(JSON.stringify({success: false, msg: "保存失败！"}));
				return ;
			}

			detail.forEach(el => {
				sql = "INSERT INTO tb_role_permission (rid, menuid, enabled) VALUES (" + rid + ", " + el.menuId + ", " + el.enabled + ")";
				db.executeSql(sql, err => {
					
				});
			});
			res.send(JSON.stringify({success: true, msg: "保存成功！"}));
		});
	} else {
		res.send(JSON.stringify({success: false, msg: "参数错误！"}));
	}
});

module.exports = router;

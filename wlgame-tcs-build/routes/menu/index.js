let express = require('express');
let router = express.Router();
let db = require("../../server/libs/db");

/* GET home page. */
router.get('/', function (req, res, next) {
	let page = req.query.page || 1;
	let pageSize = 10;
	let startIndex = (page - 1) * pageSize;
    let data = {
        module: "菜单管理", 
        title: '菜单列表',
        allMenus: [],
        allRoots: []
    };
	let sql = "SELECT COUNT(*) totalCount FROM tb_menu";
	db.executeSql(sql, function (err, ds, fields) {
		if (err) {
			res.render('branch/index', data);
			return;
		}

		if (ds.length === 0) {
			res.render('branch/index', data);
			return;
		}

		let totalCount = ds[0].totalCount;
		let totalPage = Math.ceil(totalCount / pageSize);

		sql = "SELECT t1.id, t1.mid, t1.text, t2.text root, t1.link, t1.icon, t1.enabled, t1.createtime FROM tb_menu t1 JOIN tb_menu t2 WHERE t1.root = t2.mid ORDER BY t2.text, t1.text LIMIT " + startIndex + ", " + pageSize + ";";
		db.executeSql(sql, function (err, ds, fields) {
			if (err) {
				res.render("menu/index", data);
				return ;
			}

			var allMenus = [];
			for (var i = 0; i < ds.length; i++) {
				var menu = {};
				menu.id = ds[i].id;
				menu.order = i + 1;
				menu.text = ds[i].text;
				menu.mid = ds[i].mid;
				menu.link = ds[i].link;
				menu.icon = ds[i].icon;
				menu.enabled = ds[i].enabled;
				menu.createtime = ds[i].createtime;
				menu.root = ds[i].root;

				allMenus.push(menu);
			}
			data.allMenus = allMenus;

			var sql = "SELECT * FROM tb_menu t WHERE t.root = 1 or t.root = 0 ORDER BY t.mid";
			db.executeSql(sql, function (err, ds, fields) {
				if (err) {
					res.render("menu/index", data);
					return;
				}
				var allRoots = [];
				for (var i = 0; i < ds.length; i++) {
					var menu = {};
					menu.id = ds[i].id;
					menu.order = i + 1;
					menu.text = ds[i].text;
					menu.mid = ds[i].mid;
					menu.link = ds[i].link;
					menu.icon = ds[i].icon;
					menu.enabled = ds[i].enabled;
					menu.createtime = ds[i].createtime;
					menu.root = ds[i].root;
					allRoots.push(menu);
				}
				data.allRoots = allRoots;
				data.lastPage = parseInt(page) - 1;
				data.currentPage = parseInt(page);
				data.nextPage = parseInt(page) + 1;
				data.maxPage = totalPage;
				data.navUrl = "/menu/";
				res.render("menu/index", data);
			});
		});
	});
});

router.get("/sidebar", function (req, res, next) {
    res.render('menu/sidebar', { module: "菜单管理", title: '菜单列表' });
});

router.get("/updateMenu", function (req, res, next) {
    console.log("updateMenu", req.query);
	let menuId = parseInt(req.query.menuId);
	let menuName = req.query.menuName;
	let menuRoot = parseInt(req.query.menuRoot);
	let menuLink = req.query.menuLink;
	let menuIcon = req.query.menuIcon;
	let menuEnabled = req.query.menuEnabled ? 1 : 0;
	let isUpdate = req.query.isUpdate;

	let data = {
		mid: menuId,
		text: menuName,
		root: menuRoot,
		link: menuLink,
		icon: menuIcon,
		enabled: menuEnabled
	};
	if (isUpdate) {
		let indexData = {
			mid: menuId
		};
		db.updateTableRecord("tb_menu", indexData, data, function (err, ds, fields) {
			if (err) {
				res.send(JSON.stringify({ success: false, msg: "保存失败，请稍候重试！" }));
				return;
			}
			res.send(JSON.stringify({ success: true, msg: "保存成功！" }));
		});
	} else {
		let sql = "SELECT * FROM tb_menu where mid=" + menuId;
		db.executeSql(sql, function (err, ds, fields) {
			if (err) {
				console.log("/addMenu", err);
				res.send(JSON.stringify({ success: false, msg: "系统错误" }));
				return;
			}

			if (ds.length > 0) {
				console.log("菜单ID已经存在");
				res.send(JSON.stringify({ success: false, msg: "菜单ID已经存在" }));
				return;
			}
			db.insertTableRecord("tb_menu", data, function (err, ds, fields) {
				if (err) {
					res.send(JSON.stringify({ success: false, msg: "保存失败，请稍候重试！" }));
					return;
				}
				res.send(JSON.stringify({ success: true, msg: "保存成功！" }));
			});
		});
	}
});

router.get("/delMenu", function (req, res, next) {
	let mid = req.query.mid;
	if (!mid || mid.length === 0) {
		console.log("参数mid不能为空");
		res.send(JSON.stringify({success: false, msg: "参数mid不能为空"}));
		return ;
	}

	let sql = "DELETE FROM tb_menu WHERE mid = '" + mid + "'";
	db.executeSql(sql, function (err, ds, fields) {
		if (err) {
			console.log(err);
			res.send(JSON.stringify({success: false, msg: "删除记录失败"}));
			return ;
		}

		res.send(JSON.stringify({success: true, msg: "删除记录成功"}));
	});
});

router.get("/getMenu", function (req, res, next) {
	let mid = req.query.mid;
	if (!mid || mid.length === 0) {
		res.send(JSON.stringify({success: false, msg: "参数mid不能为空"}));
		return ;
	}

	let sql = "SELECT * FROM tb_menu t WHERE t.mid = '" + mid + "'";
	db.executeSql(sql, function (err, ds, fields) {
		if (err) {
			res.send(JSON.stringify({success: false, msg: "查询记录失败"}));
			return;
		}

		if (ds.length === 0) {
			res.send(JSON.stringify({success: false, msg: "记录不存在"}));
			return ;
		}
		let data = {
			mid: ds[0].mid,
			text: ds[0].text,
			root: ds[0].root,
			link: ds[0].link,
			icon: ds[0].icon,
			enabled: ds[0].enabled
		};
		res.send(JSON.stringify({success: true, msg: "", data: data}));
	});
});

let formatUserMenus = function (ds) {
	let allMenus = {};
	for (let idx in ds) {
		if (ds[idx].root == 1) {
			let rootMid = ds[idx].rootMid;
			allMenus[rootMid] = [];
		}
	}
	for (let idx in ds) {
		if (ds[idx].enabled == 1) {
			let rootMid = ds[idx].rootMid;
			let menu = {};
			menu.isTitle = false;
			menu.mid = ds[idx].mid;
			menu.text = ds[idx].text;
			menu.root = ds[idx].root;
			menu.link = ds[idx].link;
			menu.enabled = ds[idx].enabled;
			menu.icon = ds[idx].icon;
			if (parseInt(ds[idx].root) != 1) {
				allMenus[rootMid].push(menu);
			} else {
				menu.isTitle = true;
				allMenus[rootMid].unshift(menu);
			}
		}
	}
	return allMenus;
};

router.get("/getUserMenu", function (req, res, next) {
	let uid = req.session.uid;
	let rid = req.session.rid;
	let baseSql = "";
	baseSql = "select t1.mid rootMid, t1.text rootText, t2.mid mid, t2.text text, t2.root root, t2.link link, t2.icon, if(t3.enabled=1, 1, 0) AS enabled  ";
	baseSql += "from (select * from tb_menu where root = 1) t1 ";
	baseSql += "left join tb_menu t2 ";
	baseSql += "on (t1.mid = t2.root or t1.mid = t2.mid) ";

	let sql = baseSql + "left join tb_user_permission t3 on t3.uid = " + uid + " AND t3.menuid = t2.mid ORDER BY t2.mid";
	db.executeSql(sql, (err,  ds, fields) => {
		if (err) {
			res.send(JSON.stringify({success: false, msg: "查询失败！"}));
			return;
		}
		let isEmptyPermission = true;
		for (let i = 0; i < ds.length; i++) {
			if (ds[i].enabled > 0) {
				isEmptyPermission = false;
				break;
			}
		}
		if (isEmptyPermission) {
			sql = baseSql + "left join tb_role_permission t3 on t3.rid = " + rid + " AND t3.menuid = t2.mid ORDER BY t2.mid";
			db.executeSql(sql, (err, ds, fields) => {
				if (err) {
					res.send(JSON.stringify({success: false, msg: "查询失败！"}));
					return;
				}
				let myMenus = formatUserMenus(ds);
				res.send(JSON.stringify({success: true, msg: "获取菜单成功", data: myMenus}));
			});
			return ;
		} else {
			let myMenus = formatUserMenus(ds);
			res.send(JSON.stringify({success: true, msg: "获取菜单成功", data: myMenus}));
		}
	});
});

module.exports = router;

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
	if (req.session.loginInfo != null) {
		let data = {
			title: '未来同城赛打包系统',
			username: req.session.username || ""
		};
		res.render("admin", data);
		return ;
	}
    res.render('login/login', { title: '未来同城赛打包系统' });
});

module.exports = router;
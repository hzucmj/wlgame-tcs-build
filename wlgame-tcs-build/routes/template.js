var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('branch/index', { module: "模块名称", title: '功能项名称' });
});

module.exports = router;

var express = require('express');
var router = express.Router();
var db = require("../../server/libs/db");
var MD5 = require("../../server/libs/MD5");


router.get('/', function (req, res, next) {
    let page = req.query.page || 1;
    let pageSize = 10;
    let startIndex = (page - 1) * pageSize;

	var data = {
        module: "用户管理", 
        title: '用户列表',
        allUsers: [],
        roleItems: {},
    };

	let sql = "SELECT COUNT(*) totalCount FROM tb_role";
	db.executeSql(sql, function (err, ds, fields) {
		if (err) {
			res.render('user/userIndex', data);
			return;
		}

		if (ds.length === 0) {
			res.render('user/userIndex', data);
			return;
		}

		let totalCount = ds[0].totalCount;
		let totalPage = Math.ceil(totalCount / pageSize);

	    sql = "SELECT rid, role FROM tb_role ORDER BY rid ASC";
	    db.executeSql(sql, function (err, ds, fields) {
	        if (!err) {
	            data.roleItems = ds;

	            sql = "SELECT * FROM tb_user LIMIT " + startIndex + "," + pageSize + ";"
	            db.executeSql(sql, function (err, ds, fields) {
	                if (err) {
	                    res.render('user/userIndex', data);
	                    return;
	                }

	                var allUsers = [];
	                for (var i = 0; i < ds.length; i++) {
	                    var user = {};
	                    user.uid = ds[i].uid;
	                    user.name = ds[i].name;
	                    user.role = ds[i].role;
	                    user.enabled = ds[i].enabled;

	                    var date = new Date(ds[i].createtime);
	                    var dateFormat = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
	                    user.createtime = dateFormat;

	                    allUsers.push(user);
	                }
	                data.allUsers = allUsers;
	                data.lastPage = parseInt(page) - 1;
	                data.currentPage = parseInt(page);
	                data.nextPage = parseInt(page) + 1;
	                data.maxPage = totalPage;
	                data.navUrl = "/user/";
	                res.render('user/userIndex', data);
	            });
	        }
	    });
	});
});

router.get('/searchUser', function (req, res, next) {
	let uid = req.query.uid;
	let keyword = req.query.keyword;
	let sql = "SELECT * FROM tb_user WHERE 1=1";
	if (uid && uid.length > 0) {
		sql += " AND uid = " + uid;
	}
	if (keyword && keyword.length > 0) {
		sql += " AND name like '%" + keyword + "%'";
	}
    db.executeSql(sql, function (err, ds, fields) {
    	if (err) {
    		res.send(JSON.stringify({success: false, msg: "查询用户失败"}));
    		return ;
    	}

    	ds.forEach(el => {
    		if (el.password) {
    			delete(el.password);
    		}
    	});
        res.send(JSON.stringify({success: true, msg: "查询用户成功", data: ds}));
    });
});

router.get('/addUser', function (req, res, next) {
	req.query.password = MD5.encrypt(req.query.password);
	db.insertTableRecord("tb_user", req.query, function (err, ds, fields) {
        if (err) {
            res.send(JSON.stringify({ success: false, msg: "添加用户失败，请稍候重试！" }));
            return;
        }

        res.send(JSON.stringify({ success: true, msg: "添加用户成功！" }));
    });
});

router.get('/editUser', function (req, res, next) {
	let pwd = req.query.data.password;
	if (!pwd || pwd.length == 0) {
		req.query.data.password =  null;
	} else {
		req.query.data.password = MD5.encrypt(req.query.data.password);
	}
    db.updateTableRecord("tb_user", req.query.indexData, req.query.data, function (err, ds, fields) {
        if (err) {
            res.send(JSON.stringify({ success: false, msg: "编辑用户失败，请稍候重试！" }));
            return;
        }

        res.send(JSON.stringify({ success: true, msg: "编辑用户成功！" }));
    });
});

router.get('/deleteUser', function (req, res, next) {
	var sql = "DELETE FROM tb_user WHERE uid = " + req.query.uid;
    db.executeSql(sql, function (err, ds, fields) {
        if (err) {
            res.send(JSON.stringify({ success: false, msg: "删除用户失败，请稍候重试！" }));
            return;
        }

        res.send(JSON.stringify({ success: true, msg: "删除用户成功！" }));
    });
});

router.post("/login", function (req, res, next) {
	let username = req.body.username;
	let password = req.body.password;
	console.log(username, password);
	var sql = `SELECT * FROM tb_user WHERE name='${username}'`;
	db.executeSql(sql, function (err, ds, fields) {
		if (err) {
			res.send(JSON.stringify({success: false, msg: "登录失败，查询出错！"}));
			return;
		} 

		if (ds.length === 0) {
			res.send(JSON.stringify({success: false, msg: "账号不存在！"}));
			return ;
		}

		let pwd = ds[0].password;
		if (pwd != MD5.encrypt(password)) {
			res.send(JSON.stringify({success: false, msg: "账号密码错误！"}));
			return;
		}
		req.session.loginInfo = MD5.encrypt(username+pwd);
		req.session.username = username;
		req.session.userrole = ds[0].role;
		req.session.uid = ds[0].uid;
		req.session.rid = ds[0].role;
		res.send(JSON.stringify({success: true, msg: "登录成功！"}));
	});
});

router.get("/logout", function (req, res, next) {
	req.session.loginInfo = null;
	res.redirect("/");
});

module.exports = router;

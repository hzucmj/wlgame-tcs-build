var express = require('express');
var router = express.Router();
var db = require("../../server/libs/db");

router.get('/', function (req, res, next) {
    let page = req.query.page || 1;
    let pageSize = 10;
    let startIndex = (page - 1) * pageSize;

    var data = {
        module: "用户组管理", 
        title: '用户组列表',
        allRoles: [],
    };


    let sql = "SELECT COUNT(*) totalCount FROM tb_role";
	db.executeSql(sql, function (err, ds, fields) {
		if (err) {
			res.render('user/roleIndex', data);
			return;
		}

		if (ds.length === 0) {
			res.render('user/roleIndex', data);
			return;
		}

		let totalCount = ds[0].totalCount;
		let totalPage = Math.ceil(totalCount / pageSize);

	    sql = "SELECT * FROM tb_role LIMIT " + startIndex + "," + pageSize + ";"
	    db.executeSql(sql, function (err, ds, fields) {
	        if (err) {
	            res.render('user/roleIndex', data);
	            return;
	        }

	        var allRoles = [];
	        for (var i = 0; i < ds.length; i++) {
	            var role = {};
	            role.rid = ds[i].rid;
	            role.role = ds[i].role;
	            role.enabled = ds[i].enabled;

	            var date = new Date(ds[i].createtime);
	            var dateFormat = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
	            role.createtime = dateFormat;

	            allRoles.push(role);
	        }
	        data.allRoles = allRoles;
	        data.lastPage = parseInt(page) - 1;
	        data.currentPage = parseInt(page);
	        data.nextPage = parseInt(page) + 1;
	        data.maxPage = totalPage;
	        data.navUrl = "/role/";
	        res.render('user/roleIndex', data);
	    });
	});
});

router.get('/addRole', function (req, res, next) {
    db.insertTableRecord("tb_role", req.query, function (err, ds, fields) {
        if (err) {
            res.send(JSON.stringify({ success: false, msg: "添加用户组失败，请稍候重试！" }));
            return;
        }

        res.send(JSON.stringify({ success: true, msg: "添加用户组成功！" }));
    });
});

router.get('/editRole', function (req, res, next) {
    db.updateTableRecord("tb_role", req.query.indexData, req.query.data, function (err, ds, fields) {
        if (err) {
            res.send(JSON.stringify({ success: false, msg: "编辑用户组失败，请稍候重试！" }));
            return;
        }

        res.send(JSON.stringify({ success: true, msg: "编辑用户组成功！" }));
    });
});

router.get('/deleteRole', function (req, res, next) {
	var sql = "DELETE FROM tb_role WHERE rid = " + req.query.rid;
    db.executeSql(sql, function (err, ds, fields) {
        if (err) {
            res.send(JSON.stringify({ success: false, msg: "删除用户组失败，请稍候重试！" }));
            return;
        }

        res.send(JSON.stringify({ success: true, msg: "删除用户组成功！" }));
    });
});

router.get("/searchRole", function (req, res, next) {
	let rid = req.query.rid;
	let sql = "SELECT * FROM tb_role";
	if (rid && rid.length > 0) {
		sql += " WHERE rid = " + rid;
	}
	db.executeSql(sql, function (err, ds, fields) {
		if (err) {
            res.send(JSON.stringify({ success: false, msg: "查询用户组失败，请稍候重试！" }));
			return ;
		}

		if (ds && ds.length == 0) {
            res.send(JSON.stringify({ success: false, msg: "查询用户组失败，请稍候重试！" }));
			return ;
		}

        res.send(JSON.stringify({ success: true, msg: "查询用户组成功！", data: ds }));
	});
});

module.exports = router;

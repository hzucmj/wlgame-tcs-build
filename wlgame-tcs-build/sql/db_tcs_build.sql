/*
Navicat MySQL Data Transfer

Source Server         : 123.207.245.72
Source Server Version : 50718
Source Host           : 123.207.245.72:3306
Source Database       : db_tcs_build

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2018-11-19 16:48:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_branch
-- ----------------------------
DROP TABLE IF EXISTS `tb_branch`;
CREATE TABLE `tb_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repostitory` int(11) DEFAULT NULL COMMENT '0:gamehall,1:client-lobby,2:mahjoingUI',
  `branch` varchar(255) DEFAULT NULL,
  `giturl` text,
  `type` int(11) DEFAULT '0',
  `description` text,
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_branch
-- ----------------------------
INSERT INTO `tb_branch` VALUES ('1', '0', 'platform', 'https://gitee.com/115088853/game-hall-client-new.git', '0', '11月p0-俱乐部，红包赛', '2018-11-05 18:16:35');
INSERT INTO `tb_branch` VALUES ('2', '1', 'develop_v6.0.2', 'https://gitee.com/115088853/client-lobby.git', '1', '11月p0-俱乐部，红包赛', '2018-11-05 18:16:35');
INSERT INTO `tb_branch` VALUES ('5', '0', 'platform', null, '0', '引擎线上分支（勿删）', '2018-11-13 17:27:20');
INSERT INTO `tb_branch` VALUES ('6', '1', 'platform', null, '1', '大厅线上分支（勿删）', '2018-11-13 17:27:44');

-- ----------------------------
-- Table structure for tb_build_cfg
-- ----------------------------
DROP TABLE IF EXISTS `tb_build_cfg`;
CREATE TABLE `tb_build_cfg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `version` varchar(64) DEFAULT NULL,
  `params` text COMMENT 'json格式',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '打包类型：0apk，1大厅热更包，2子游戏热更包',
  `enabled` int(11) DEFAULT '1',
  `description` text,
  `creator` varchar(64) DEFAULT NULL,
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_build_cfg
-- ----------------------------
INSERT INTO `tb_build_cfg` VALUES ('14', 'develop_v6.0.2-测试环境', 'v_6.0.2', '{\"platform\":\"\",\"mode\":\"debug\",\"gamehallbranch\":\"platform\",\"lobbybranch\":\"develop_v6.0.2\",\"env\":\"test\",\"forceclean\":\"0\",\"email\":\"\",\"subhead\":\"\",\"fir_api_token\":\"232dd0b3c85208a1900b72d6487fb142\",\"pngquant\":\"1\"}', '0', '1', '11月p0-俱乐部，红包赛', 'admin', '2018-11-15 16:45:43');

-- ----------------------------
-- Table structure for tb_fir
-- ----------------------------
DROP TABLE IF EXISTS `tb_fir`;
CREATE TABLE `tb_fir` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` text NOT NULL,
  `owner` varchar(64) DEFAULT NULL,
  `enabled` int(11) DEFAULT '1',
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_fir
-- ----------------------------

-- ----------------------------
-- Table structure for tb_menu
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu`;
CREATE TABLE `tb_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(64) NOT NULL,
  `text` varchar(64) DEFAULT NULL,
  `root` varchar(64) DEFAULT NULL,
  `link` text,
  `icon` text,
  `enabled` int(11) DEFAULT '1',
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `key_UNIQUE` (`mid`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_menu
-- ----------------------------
INSERT INTO `tb_menu` VALUES ('1', '1000', '版本构建', '1', './build/apk/', './images/sidebar/package.png', '1', '2012-12-05 09:50:03');
INSERT INTO `tb_menu` VALUES ('2', '1001', 'Android | IOS', '1000', './build/apk/', './images/sidebar/phone.png', '1', '2012-12-05 09:50:03');
INSERT INTO `tb_menu` VALUES ('3', '1', '根节点', '0', null, null, '1', '2012-12-05 09:50:03');
INSERT INTO `tb_menu` VALUES ('4', '1002', '大厅ZIP包', '1000', './build/hallzip/', './images/sidebar/zip.png', '1', '2018-11-06 16:55:47');
INSERT INTO `tb_menu` VALUES ('5', '1003', '子游戏ZIP包', '1000', './build/gamezip/', './images/sidebar/zip.png', '1', '2018-11-06 17:01:08');
INSERT INTO `tb_menu` VALUES ('6', '1004', '打包记录', '1005', './task/history/', './images/sidebar/list.png', '1', '2018-11-06 17:03:49');
INSERT INTO `tb_menu` VALUES ('7', '1005', '版本管理', '1', './version/', './images/sidebar/version.png', '1', '2018-11-06 17:05:03');
INSERT INTO `tb_menu` VALUES ('8', '1006', '分支管理', '1000', './branch/', './images/sidebar/branch.png', '1', '2018-11-06 17:05:26');
INSERT INTO `tb_menu` VALUES ('9', '1007', '用户管理', '1012', './user/', './images/sidebar/account.png', '1', '2018-11-06 17:07:10');
INSERT INTO `tb_menu` VALUES ('10', '1008', '用户组管理', '1012', './role/', './images/sidebar/account-group.png', '1', '2018-11-06 17:07:38');
INSERT INTO `tb_menu` VALUES ('11', '1009', '权限管理', '1012', './permission/', './images/sidebar/permission.png', '1', '2018-11-06 17:13:17');
INSERT INTO `tb_menu` VALUES ('12', '1010', '菜单管理', '1012', './menu/', './images/sidebar/menu.png', '1', '2018-11-06 17:13:41');
INSERT INTO `tb_menu` VALUES ('19', '1011', '开始打包', '1000', './build/allconfigs/', './images/sidebar/document.png', '1', '2018-11-14 01:18:20');
INSERT INTO `tb_menu` VALUES ('20', '1012', '系统管理', '1', './', './images/sidebar/system.png', '1', '2018-11-16 01:28:41');
INSERT INTO `tb_menu` VALUES ('21', '1013', 'APK差分', '1005', './', null, '1', '2018-11-16 01:47:30');
INSERT INTO `tb_menu` VALUES ('22', '1014', 'ZIP包差分', '1005', './', null, '1', '2018-11-16 01:47:58');
INSERT INTO `tb_menu` VALUES ('23', '1015', '打包队列', '1000', './task/', './images/sidebar/queue.png', '1', '2018-11-16 18:37:39');

-- ----------------------------
-- Table structure for tb_permission
-- ----------------------------
DROP TABLE IF EXISTS `tb_permission`;
CREATE TABLE `tb_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `rid` int(11) DEFAULT NULL,
  `menu_1000` int(11) DEFAULT NULL,
  `menu_1001` int(11) DEFAULT NULL,
  `menu_1002` int(11) DEFAULT NULL,
  `menu_1003` int(11) DEFAULT NULL,
  `menu_1004` int(11) DEFAULT NULL,
  `menu_1005` int(11) DEFAULT NULL,
  `menu_1006` int(11) DEFAULT NULL,
  `menu_1007` int(11) DEFAULT NULL,
  `menu_1008` int(11) DEFAULT NULL,
  `menu_1009` int(11) DEFAULT NULL,
  `menu_1010` int(11) DEFAULT NULL,
  `menu_1011` int(11) DEFAULT NULL,
  `menu_1012` int(11) DEFAULT NULL,
  `menu_1013` int(11) DEFAULT NULL,
  `menu_1014` int(11) DEFAULT NULL,
  `menu_1015` int(11) DEFAULT NULL,
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  UNIQUE KEY `rid` (`rid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_permission
-- ----------------------------
INSERT INTO `tb_permission` VALUES ('6', null, '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2018-11-19 10:02:16');

-- ----------------------------
-- Table structure for tb_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(64) DEFAULT NULL,
  `enabled` int(11) NOT NULL DEFAULT '1',
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`rid`),
  UNIQUE KEY `rid_UNIQUE` (`rid`),
  UNIQUE KEY `role_UNIQUE` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_role
-- ----------------------------
INSERT INTO `tb_role` VALUES ('1', '管理员', '1', '2018-11-11 18:30:58');
INSERT INTO `tb_role` VALUES ('2', '前端开发', '1', '2018-11-13 10:47:55');
INSERT INTO `tb_role` VALUES ('3', '项目经理', '1', '2018-11-13 17:37:03');
INSERT INTO `tb_role` VALUES ('4', '测试', '1', '2018-11-13 17:37:08');
INSERT INTO `tb_role` VALUES ('5', '产品经理', '1', '2018-11-13 17:37:16');
INSERT INTO `tb_role` VALUES ('6', '服务端开发', '1', '2018-11-19 15:13:42');
INSERT INTO `tb_role` VALUES ('7', '美术', '1', '2018-11-19 15:14:09');

-- ----------------------------
-- Table structure for tb_task
-- ----------------------------
DROP TABLE IF EXISTS `tb_task`;
CREATE TABLE `tb_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskid` varchar(128) DEFAULT NULL COMMENT '任务id，后续需要任务状态刷新界面，可以用时间戳',
  `params` text COMMENT '打包参数，json串',
  `type` int(11) DEFAULT '0' COMMENT '0:apk,1:hallzip,2:gamezip,决定打什么包，运行什么脚本',
  `status` int(11) DEFAULT '0' COMMENT '任务状态，0:未开始，1:正在进行中，2:已完成',
  `apkurl` text COMMENT '安卓url',
  `iosurl` text COMMENT 'ios url',
  `zipurl` text,
  `owner` varchar(64) DEFAULT NULL COMMENT '谁打的包',
  `description` text,
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `taskid_UNIQUE` (`taskid`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_task
-- ----------------------------
INSERT INTO `tb_task` VALUES ('42', 'T1542278039713', '{\"platform\":\"Android\",\"mode\":\"debug\",\"gamehallbranch\":\"platform\",\"lobbybranch\":\"develop_v6.0.2\",\"env\":\"test\",\"forceclean\":\"0\",\"email\":\"\",\"subhead\":\"\",\"fir_api_token\":\"232dd0b3c85208a1900b72d6487fb142\",\"pngquant\":\"1\"}', '0', '2', 'https://fir.im/9a23?release_id=5bed4eb8959d696b6888f84d', null, null, 'admin', '11月p0-俱乐部，红包赛', '2018-11-15 18:33:59');
INSERT INTO `tb_task` VALUES ('43', 'T1542279265955', '{\"platform\":\"Android\",\"mode\":\"debug\",\"gamehallbranch\":\"platform\",\"lobbybranch\":\"develop_v6.0.2\",\"env\":\"test\",\"forceclean\":\"0\",\"email\":\"\",\"subhead\":\"\",\"fir_api_token\":\"232dd0b3c85208a1900b72d6487fb142\",\"pngquant\":\"1\"}', '0', '2', 'https://fir.im/9a23?release_id=5bed5371ca87a818486d2676', null, null, 'admin', '11月p0-俱乐部，红包赛', '2018-11-15 18:54:25');
INSERT INTO `tb_task` VALUES ('44', 'T1542280075239', '{\"platform\":\"Android&iOS\",\"mode\":\"debug\",\"gamehallbranch\":\"platform\",\"lobbybranch\":\"develop_v6.0.2\",\"env\":\"test\",\"forceclean\":\"0\",\"email\":\"\",\"subhead\":\"\",\"fir_api_token\":\"232dd0b3c85208a1900b72d6487fb142\",\"pngquant\":\"1\"}', '2', '2', 'https://fir.im/9a23?release_id=5bed658d548b7a0359f48c1b', 'https://fir.im/ac5m?release_id=5bed69de548b7a35f7fc1586', null, 'admin', '11月p0-俱乐部，红包赛', '2018-11-15 19:07:55');
INSERT INTO `tb_task` VALUES ('45', 'T1542286961948', '{\"platform\":\"Android\",\"mode\":\"debug\",\"gamehallbranch\":\"platform\",\"lobbybranch\":\"develop_v6.0.2\",\"env\":\"test\",\"forceclean\":\"0\",\"email\":\"\",\"subhead\":\"\",\"fir_api_token\":\"232dd0b3c85208a1900b72d6487fb142\",\"pngquant\":\"1\"}', '0', '2', 'https://fir.im/9a23?release_id=5bed71c4548b7a35f7fc15f6', null, null, 'admin', '11月p0-俱乐部，红包赛', '2018-11-15 21:02:41');
INSERT INTO `tb_task` VALUES ('46', 'T1542287033672', '{\"platform\":\"Android\",\"mode\":\"debug\",\"gamehallbranch\":\"platform\",\"lobbybranch\":\"develop_v6.0.2\",\"env\":\"test\",\"forceclean\":\"0\",\"email\":\"\",\"subhead\":\"\",\"fir_api_token\":\"232dd0b3c85208a1900b72d6487fb142\",\"pngquant\":\"1\"}', '0', '6', null, null, null, 'admin', '11月p0-俱乐部，红包赛', '2018-11-15 21:03:53');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(64) DEFAULT NULL,
  `enabled` int(11) NOT NULL DEFAULT '1',
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uid_UNIQUE` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('2', 'admin', '21232f297a57a5a743894a0e4a801fc3', '1', '0', '2018-11-13 23:42:11');
INSERT INTO `tb_user` VALUES ('3', 'bryan', 'c6e5064673e0dfdba0c0202f33dc7192', '2', '0', '2018-11-14 01:06:14');
INSERT INTO `tb_user` VALUES ('4', 'vaclin', 'c93169f1eb9be7246f990690b5e66b2d', '1', '0', '2018-11-15 16:57:52');


$(function () {

	$("#btn-task-refresh").click(function () {
		window.location.reload();
	});

	$(".download-icon").mouseover(function (evt) {
		let isAndroid = $(evt.target.parentNode).attr("class").indexOf("android") > 0;
		let title = isAndroid ? "Android" : "IOS";
		// console.log(evt);
		if ($("#qrcode").length <= 0) {
			let html = "<div id='qrcode'></div>";
			$("body").append(html);
		} else {
			$("#qrcode").empty();
			$("#qrcode").attr("style", "display:block;");
			$("#qrcode").append("<h3 style='text-align: center; color: #CC3333'>" + title + "</h3>");
		}
		$("#qrcode").attr("style", "width: 120px; height: 120px; float: left; position: absolute; left: " + (evt.pageX - 50) + "px; top: " + (evt.pageY + 20) + "px;");

		$("#qrcode").show();
		new QRCode(document.getElementById("qrcode"), {
			text: evt.target.parentNode.href,
			width: 150,
			height: 150,
			colorDark : "#000000",
			colorLight : "#ffffff",
		});
	});

	$(".download-icon").mouseout(function () {
		if ($("#qrcode").length > 0) {
			$("#qrcode").empty();
			$("#qrcode").attr("style", "display:none;");
		}
	});

});
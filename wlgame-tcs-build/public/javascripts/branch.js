(function () {

	let editBranchId = -1;

	let getFormData = function () {
		let repostitory = $("#input-select-repostitory").val();
		let branch = $("#input-select-branch").val();
		let description = $("#input-select-description").val();
		return {
			repostitory: repostitory,
			branch: branch,
			description: description
		};
	};

	let requestSaveBranch = function (params) {
		if (!params.branch || params.branch.length === 0) {
			showTips("分支名称不能为空");
			return ;
		}

		if (!params.description || params.description.length === 0) {
			showTips("分支描述不能为空");
			return ;
		}

		$.ajax({
			url: "./saveBranch",
			data: params,
			success: function (res) {
				let data = JSON.parse(res);
				if (!data.success) {
					editBranchId = -1;
					$("#add-branch-modal").modal('hide');
					showTips(data.msg);
					return ;
				}

				editBranchId = -1;
				$("#add-branch-modal").modal('hide');
				showTips(data.msg, null, null, null, null, function () {
					window.location.reload();
				});
			},
			fail: function (res) {
				console.log(res);
				editBranchId = -1;
				$("#add-branch-modal").modal('hide');
				showTips("保存失败");
			},
			error: function () {
				editBranchId = -1;
				$("#add-branch-modal").modal('hide');
				showTips("系统错误");
			}
		})
	};

	$("#btn-add-branch").click(function () {
		$("#btn-add-branch-confirm").show();
		$("#btn-edit-branch-confirm").hide();
		$("#input-select-repostitory").find("option").attr("selected", false);
		$("#input-select-repostitory").find("option[value=0]").attr("selected", true);
		$("#input-select-branch").attr("value", "");
		$("#input-select-description").attr("value", "");
		$("#add-branch-modal").modal();
	});

	$("#btn-add-branch-confirm").click(function () {
		// 请求保存
		let formData = getFormData();
		requestSaveBranch(formData);
	});

	$(".btn-edit-branch").click(function (evt) {
		// 请求保存
		let target = evt.target;
		if (!target) {
			return ;
		}
		let id = target.id;
		let bid = id.substring(id.lastIndexOf("-") + 1);
		editBranchId = bid;
		$.ajax({
			url: "./getBranch",
			data: {
				id: bid
			},
			success: function (res) {
				let data = JSON.parse(res);

				if (!data.success) {
					editBranchId = -1;
					$("#add-branch-modal").modal('hide');
					showTips(data.msg);
					return ;
				}

				data = data.data;
				$("#btn-add-branch-confirm").hide();
				$("#btn-edit-branch-confirm").show();
				$("#input-select-repostitory").find("option[value=" + data.repostitory + "]").attr("selected", true);
				$("#input-select-branch").attr("value", data.branch);
				$("#input-select-description").attr("value", data.description);
				$("#add-branch-modal").modal();
			},
			fail: function (res) {
				editBranchId = -1;
				$("#add-branch-modal").modal('hide');
				showTips("查询记录失败");
			},
			error: function () {
				editBranchId = -1;
				$("#add-branch-modal").modal('hide');
				showTips("系统错误");
			}
		});
	});

	$("#btn-edit-branch-confirm").click(function (evt) {
		let formData = getFormData();
		formData.isUpdate = true;
		formData.id = editBranchId;
		requestSaveBranch(formData);
	});

	$(".btn-del-branch").click(function (evt) {
		let target = evt.target;
		if (!target) {
			return ;
		}
		let id = target.id;
		let bid = id.substring(id.lastIndexOf("-") + 1);
		editBranchId = bid;

		showTips("确定要删除该分支吗？", null, "确定", function () {
			$.ajax({
				url: "./delBranch",
				data: {
					id: bid
				},
				success: function (res) {
					let data = JSON.parse(res);
					if (!data.success) {
						editBranchId = -1;
						$("#add-branch-modal").modal('hide');
						showTips(data.msg);
						return ;
					}ß
					editBranchId = -1;
					$("#add-branch-modal").modal('hide');
					showTips(data.msg, null, null, null, null, function () {
						window.location.reload();
					});
				},
				fail: function (res) {ß
					console.log(res);
					editBranchId = -1;
					$("#add-branch-modal").modal('hide');
					showTips("删除失败");
				},
				error: function (res) {
					editBranchId = -1;
					$("#add-branch-modal").modal('hide');
					showTips("系统错误");
				}
			})
		}, "取消", function () {

		});
	});
})();
$(function () {

	currentRoldId = null;
	currentUid = null;

	let queryPermission = function (type, rid, uid) {
		$.ajax({
			url: "/permission/queryPermission", 
			data: {
				rid: rid,
				type: type,
				uid: uid
			},
			dataType: "json", 
			success: function (res) {
				// console.log(res);
				if (!res.success) {
					showTips(res.msg);
					return ;
				}

				let theader = $("#table-header-submenu");
				theader.attr("colspan", res.data.maxItemCount - 1);

				let tbody = $("#table-menu-permission");
				tbody.html("");
				let menus = res.data.menus || {};
				let html = "";
				for (key in menus) {
					let items = menus[key];
					html += "<tr>";
					items.forEach((menu) => {
						if (menu.isTitle) {
							html += `<td style="text-align:center;"><b>${menu.text}</b></td>`
						} else {
							if (menu.enabled) {
								html += `<td style="text-align:left;"><input type="checkbox" class="item-selete-status checkbox-menu-permission" id="checkbox-menu-permission-${menu.mid}" checked="checked" />&nbsp;${menu.text}</td>`;
							} else {
								html += `<td style="text-align:left;"><input type="checkbox" class="item-selete-status" id="checkbox-menu-permission-${menu.mid}" />&nbsp;${menu.text}</td>`;
							}
						}
					});
					html += "</tr>";
				}

				tbody.append(html);
			},
			fail: function (res) {
				showTips("查询失败");
			},
			error: function (res) {
				showTips("系统错误！");
			}
		});
	}

	let requestRolePermission = function (rid) {
		currentRoldId = rid;
		currentUid = null;
		queryPermission(0, rid, null);
	};

	let onUserRoleItemClicked = function (evt) {
		$(".user-role-item").each((idx, el) => {
			$("#" + el.id).removeClass("active");
		});
		$(".search-user-item").each((idx, el) => {
			$("#" + el.id).removeClass("active");
		});

		let id = $(this).attr("id");
		let rid = id.substring(id.lastIndexOf("-") + 1);
		$(this).addClass("active");
		requestRolePermission(rid);
	};

	let rquestAllRole = function () {
		$.ajax({
			url: "/role/searchRole",
			data: {},
			dataType: "json",
			success: function (res) {
				if (!res.success) {
					showTips(res.msg);
					return;
				}
				$("#user-role-list").html("");
				res.data.forEach( (el, idx) => {
					let node = `<a class="list-group-item list-group-item-action user-role-item" id="user-role-item-${el.rid}">${el.role}</a>`;
					$("#user-role-list").append(node);
				});

				$(".user-role-item").click(onUserRoleItemClicked);

				requestRolePermission(1);
				$("#user-role-item-1").addClass("active");
			},
			fail: function (res) {
				showTips(res.msg);
			},
			error: function () {
				showTips("系统错误！");
			}
		});
	};

	let onUserItemClicked = function (evt) {
		$(".user-role-item").each((idx, el) => {
			$("#" + el.id).removeClass("active");
		});
		$(".search-user-item").each((idx, el) => {
			$("#" + el.id).removeClass("active");
		});
		$(this).addClass("active");

		let id = $(this).attr("id");
		let uid = id.substring(id.lastIndexOf("-") + 1);
		currentUid = uid;
		currentRoldId = null;
		queryPermission(1, null, uid);
	};

	let requestUserList = function (keyword) {
		$.ajax({
			url: "/user/searchUser",
			data: {
				keyword: keyword
			},
			dataType: "json", 
			success: function (res) {
				if (!res.success) {
					showTips(res.msg);
					return ;
				}
				// console.log(res);
				$("#user-search-list").html("");
				res.data.forEach((el, index) => {
					let node = `<a class="list-group-item list-group-item-action search-user-item" id="user-item-${el.uid}">${el.name}</a>`;
					$("#user-search-list").append(node);
				});

				$(".search-user-item").click(onUserItemClicked)
			},
			fail: function (res) {
				showTips(res.msg);
			},
			error: function () {
				showTips("系统错误！");
			}
		});
	};

	let onBtnSearchUserClicked = function (evt) {
		let keyword = $("#input-text-username").val();
		if (isEmptyString(keyword)) {
			showTips("请输入用户名");
			return ;
		}
		requestUserList(keyword);
	};

	let onBtnSaveUserPermissionClicked = function (evt) {
		let checkboxes = $("#table-menu-permission").find("input[type=checkbox]");
		let data = {};
		if (currentRoldId != null) {
			data.rid = currentRoldId;
		} else {
			data.uid = currentUid;
		}

		data.detail = [];
		checkboxes.each((idx, el) => {
			let id = el.id;
			let menuId = id.substring(id.lastIndexOf("-") + 1);
			let isChecked = el.checked;

			let d = {
				menuId: menuId,
				enabled: isChecked
			};
			data.detail.push(d);
		});

		$.ajax({
			url: "/permission/savePermission",
			data: data,
			dataType: "json",
			success: function (res) {
				if (!res.success) {
					showTips(res.msg);
					return ;
				}

				showTips(res.msg);
			},
			fail: function() {
				showTips("保存失败！");
			},
			error: function () {
				showTips("系统错误！");
			}
		})
	};

	$("#btn-search-user").click(onBtnSearchUserClicked);

	$("#btn-save-menu-permission").click(onBtnSaveUserPermissionClicked);

	let initPage = function () {
		rquestAllRole();
	};

	initPage();
});
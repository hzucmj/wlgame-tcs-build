$(function (){
	// 新增用户组
	$("#btn-add-role").click(function () {
		var rolename = $("#input-menu-rolename").val();
        var data = {
        	role:rolename,
        	enabled:0,
        }

        if(rolename == ""){
        	showTips("请输入用户组名");
        	return;
        }

        $.ajax({
        	url: "../role/addRole",
            data: data,
            success: function (res) {
                console.log(res);
                var data = JSON.parse(res);
                if (!data.success) {
                    showTips(data.msg);
                    return;
                }

                $("#btn-add-cancel").click();
                showTips(data.msg, null, null, null, null, function () {
	                location.reload();
	            });
            },
            fail: function (res) {
                console.log(res);
                showTips("操作失败，请稍候重试!");
            },
            error: function (res) {
                console.log(res);
                showTips("系统错误!");
            }
        });
	});

	// 修改用户资料
	$("#btn-edit-role").click(function () { 
        var rolename = $("#input-edit-rolename").val();
        var rolerid = $("#edit-role-modal-label").attr("name");
        var data = {
        	indexData:{rid:rolerid},
        	data:{
	        	role:rolename,
        	},
        }

        if(rolename == ""){
        	showTips("请输入用户名");
        	return;
        }

		$.ajax({
        	url: "../role/editRole",
            data: data,
            success: function (res) {
                console.log(res);
                var data = JSON.parse(res);
                if (!data.success) {
                    showTips(data.msg);
                    return;
                }

                $("#btn-edit-cancel").click();
                showTips(data.msg, null, null, null, null, function () {
                	location.reload();
                });
            },
            fail: function (res) {
                console.log(res);
                showTips("操作失败，请稍候重试!");
            },
            error: function (res) {
                console.log(res);
                showTips("系统错误!");
            }
        });
	});

	// 编辑用户
	$("button[name='roleEdit']").click(function() {
		$.ajax({
			url: "../role/searchRole",
			data: {
				rid: $(this).val()
			},

			success: function (res) {
                var data = JSON.parse(res);
                if (!data.success) {
                    showTips(data.msg);
                    return;
                }

                var roleName = data.data[0].role;
				$("#input-edit-rolename").val(roleName);
			}
		});
		$("#edit-role-modal-label").attr("name", $(this).val());
	});

	// 删除用户
	$("button[name='roleDelete']").click(function() {
		var data = {rid:$(this).val()};
		$.ajax({
        	url: "../role/deleteRole",
            data: data,
            success: function (res) {
                console.log(res);
                var data = JSON.parse(res);
                if (!data.success) {
                    showTips(data.msg);
                    return;
                }

                showTips(data.msg, null, null, null, null, function () {
	                location.reload();
	            });	
            },
            fail: function (res) {
                console.log(res);
                showTips("操作失败，请稍候重试!");
            },
            error: function (res) {
                console.log(res);
                showTips("系统错误!");
            }
        });
	});
})
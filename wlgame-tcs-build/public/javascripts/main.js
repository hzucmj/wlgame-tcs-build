$(function () {
	$("#main-web-frame").css("height", $("#nav-sidebar").height());

	window.showTips = function (msg, title, confirmTitle, fnConfirm, cancelTitle, fnCancel) {
		title = title || "提示";
		msg = msg || "";
		confirmTitle = confirmTitle || "确定";
		cancelTitle = cancelTitle || "确定";
		fnCancel = fnCancel || new Function();
		let id = "alert-dialog-" + (new Date().getTime());
		let html = "" +
			"<div class='modal fade' tabindex='-1' role='dialog' id='" + id + "'>" +
			"    <div class='modal-dialog' role='document'>" +
			"        <div class='modal-content'>" +
			"            <div class='modal-header'>" +
			"                <h5 class='modal-title'>" + title + "</h5>" +
			"                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>" +
			"                    <span aria-hidden='true'>&times;</span>" +
			"                </button>" +
			"            </div>" +
			"            <div class='modal-body'>" +
			"                <p>" + msg + "</p>" +
			"            </div>" +
			"            <div class='modal-footer'>";
		if (fnConfirm) {
			html += "                <button  type='button' class='btn btn-primary' id='btn-confirm-dialog' data-dismiss='modal'>" + confirmTitle + "</button>";
			html += "                <button type='button' class='btn btn-secondary' id='btn-cancel-dialog' data-dismiss='modal'>" + cancelTitle + "</button>";
		} else {
			html += "                <button type='button' class='btn btn-primary' id='btn-cancel-dialog' data-dismiss='modal'>" + cancelTitle + "</button>";
		}
		html += "" +
			"            </div>" +
			"        </div>" +
			"    </div>" +
			"</div>";

		$("body").append(html);

		$("#" + id).modal();

		$("#" + id).find("button[id=btn-confirm-dialog]").click(function () {
			fnConfirm();
		});
		$("#" + id).find("button[id=btn-cancel-dialog]").click(function () {
			fnCancel();
		});
	};

	window.isEmptyString = function (obj) {
		if (!obj) {
			return true;
		}

		if (typeof(obj) === "string" && obj.length === 0) {
			return true;
		}

		return false;
	}
});
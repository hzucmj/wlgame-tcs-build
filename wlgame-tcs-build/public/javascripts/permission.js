$(function (){
	$(".role-item").click(function(){
		$(".search-user-item").attr("class", "list-group-item list-group-item-action search-user-item");
		$(".role-item").attr("class", "list-group-item list-group-item-action role-item");
		$(this).attr("class", "list-group-item list-group-item-action active role-item");
		$(".seleted-show").attr("value", $(this).attr("value"));
		$(".seleted-show").attr("id", "role_config");

		let searchIndex = $(this).attr("value");
		let data = {
        	index:searchIndex,
        }
		$.ajax({
			url: "../permission/searchRoleConfig",
            data: data,
            success: function (res) {
                let menuList = res[0];

                for (var i = 0; i < $(".item-selete-status").length; i++) {
					let menuId = $(".item-selete-status").eq(i).attr('id');
					let inputTag = $(".item-selete-status").eq(i);
					let status = inputTag.is(':checked');

					if (menuList && menuList[menuId]){
						inputTag.prop("checked", menuList[menuId] == 1 ? true : false);
					}else{
						inputTag.prop("checked", false);
					}
				}
            },
            fail: function (res) {
                console.log(res);
                alert("操作失败，请稍候重试!");
            },
            error: function (res) {
                console.log(res);
                alert("系统错误!");
            }
		});
	});

	$("#save-config").click(function(){
        let permissions = {};
		for (var i = 0; i < $(".item-selete-status").length; i++) {
			let menuId = $(".item-selete-status").eq(i).attr('id');
			let status = $(".item-selete-status").eq(i).is(':checked');
			permissions[menuId] = status ? 1 : 0;
		}

		if(!$(".seleted-show").attr("value")){
			showTips("请选择用户组或用户");
		}

		let configType = $(".seleted-show").attr("id");
		if (configType == "role_config"){
			let data = {};
	        let ridIndex = $(".seleted-show").attr("value");
	        data.updateIndex = {rid:ridIndex};
	        data.updateData = permissions;

	        $.ajax({
				url: "../permission/saveRoleConfig",
	            data: data,
	            success: function (res) {
	            	console.log(res);
	            	let data = JSON.parse(res);
	                showTips(data.msg);
	            },
	            fail: function (res) {
	                console.log(res);
	                alert("操作失败，请稍候重试!");
	            },
	            error: function (res) {
	                console.log(res);
	                alert("系统错误!");
	            }
			});
		}else if(configType == "user_config"){
			let data = {};
	        let uidIndex = $(".seleted-show").attr("value");
	        data.updateIndex = {uid:uidIndex};
	        data.updateData = permissions;

	        $.ajax({
				url: "../permission/saveUserConfig",
	            data: data,
	            success: function (res) {
	                console.log(res);
	                let data = JSON.parse(res);
	                showTips(data.msg);
	            },
	            fail: function (res) {
	                console.log(res);
	                alert("操作失败，请稍候重试!");
	            },
	            error: function (res) {
	                console.log(res);
	                alert("系统错误!");
	            }
			});
		}
	});

	$("#btn-search").click(function(){
		let searchName = $("#input-text-username").val();
		let data = {
        	searchName:searchName,
        }

		$.ajax({
			url: "../permission/searchUser",
            data: data,
            dataType: "json",
            success: function (res) {
                console.log(res);

                $(".search-user-list").empty();

                for(x in res.data){
                	let addHtml = '<a class="list-group-item list-group-item-action search-user-item" onclick="userItemClick(' + res.data[x].uid +')" value="' + res.data[x].uid + '">' + res.data[x].name + '</a>';
                	$(".search-user-list").append(addHtml);
                }
            },
            fail: function (res) {
                console.log(res);
                alert("操作失败，请稍候重试!");
            },
            error: function (res) {
                console.log(res);
                alert("系统错误!");
            }
		});
	});

	// 默认点击第一个用户组
	$(".role-item").eq(0).click()
})

function userItemClick(userIndex){
	$(".role-item").attr("class", "list-group-item list-group-item-action role-item");
	$(".search-user-item").attr("class", "list-group-item list-group-item-action search-user-item");
	$(".search-user-item[value=" + userIndex + "]").attr("class", "list-group-item list-group-item-action search-user-item active");
	$(".seleted-show").attr("value", userIndex);
	$(".seleted-show").attr("id", "user_config");

	let data = {
    	index:userIndex,
    }
	$.ajax({
		url: "../permission/searchUserConfig",
        data: data,
        success: function (res) {
            let menuList = res[0];

            for (var i = 0; i < $(".item-selete-status").length; i++) {
				let menuId = $(".item-selete-status").eq(i).attr('id');
				let inputTag = $(".item-selete-status").eq(i);
				let status = inputTag.is(':checked');

				if (menuList && menuList[menuId]){
					inputTag.prop("checked", menuList[menuId] == 1 ? true : false);
				}else{
					inputTag.prop("checked", false);
				}
			}
        },
        fail: function (res) {
            console.log(res);
            alert("操作失败，请稍候重试!");
        },
        error: function (res) {
            console.log(res);
            alert("系统错误!");
        }
	});
};
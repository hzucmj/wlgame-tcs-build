$(function () {

	let editConfigId = -1;


	let loadAllBranches = function () {
		$.ajax({
			url: "/branch/getAllBranch",
			data: {

			},
			success: function (res) {
				let data = JSON.parse(res);

				if (!data.success) {
					showTips(data.msg);
					return ;
				}

				let gameHallBranch = data.data.gameHallBranch || [];
				let clientLobbyBranch = data.data.clientLobbyBranch || [];

				for (let idx in gameHallBranch) {
					$("#input-select-gamehall").append("<option value='" + gameHallBranch[idx].branch + "'>" + gameHallBranch[idx].branch + "</option>");
				}

				for (let idx in clientLobbyBranch) {
					$("#input-select-clientlobby").append("<option value='" + clientLobbyBranch[idx].branch + "'>" + clientLobbyBranch[idx].branch + "</option>");
				}

				$("#input-select-gamehall").selectpicker("refresh");
				$("#input-select-clientlobby").selectpicker("refresh");
			},
			fail: function () {
				showTips("加载分支信息失败")
			},
			error: function () {
				showTips("系统错误")
			}
		})
	};
	// 加载所有分支
	loadAllBranches();

	let getFormData = function () {
		let name = $("#input-text-config-name").val();
		let version = $("#input-text-version").val();
		let description = $("#input-text-description").val();
		let mode = $("#input-select-mode").val();
		let gamehall = $("#input-select-gamehall").val();
		let clientlobby = $("#input-select-clientlobby").val();
		let env = $("#input-select-env").val();
		let forceclean = $("#input-select-forceclean").val();
		let pngquant = $("#input-select-pngquant").val();
		let firkey = $("#input-select-firkey").val();


		let params = {
			name: name,
			platform: "",
			mode: mode,
			gamehallbranch: gamehall,
			lobbybranch: clientlobby,
			env: env,
			forceclean: parseInt(forceclean),
			email: "",
			subhead: "",
			fir_api_token: firkey,
			pngquant: parseInt(pngquant),
			type: 0,
			enabled: 1,
			description: description,
			version: version
		};
		return params;
	};

	let requestSaveBuildConfig = function (params) {
		// 各种条件验证
		console.log(params);
		if (isEmptyString(params.name)) {
			showTips("配置名称不能为空");
			return ;
		}

		if (isEmptyString(params.version)) {
			showTips("版本号不能为空");
			return ;
		}

		if (isEmptyString(params.description)) {
			showTips("版本描述不能为空");
			return ;
		}

		if (isEmptyString(params.gamehallbranch)) {
			showTips("引擎分支不能为空");
			return ;
		}

		if (isEmptyString(params.lobbybranch)) {
			showTips("大厅分支不能为空");
			return ;
		}


		$("#add-build-config-modal").modal("hide");

		$.ajax({
			url: "/build/saveConfig",
			data: params,
			success: function (res) {
				editConfigId = -1;
				let data = JSON.parse(res);

				if (!data) {
					showTips(data.msg);
					return ;
				}

				showTips(data.msg, null, null, null, null, function () {
					window.location.reload();
				});
			},
			fail: function () {
				editConfigId = -1;
				showTips("保存失败");
			},
			error: function () {
				editConfigId = -1;
				showTips("系统错误");
			}
		})
	};

	$("#btn-add-build-config").click(function () {
		$("#input-text-config-name").attr("value", "");
		$("#input-text-version").attr("value", "");
		$("#input-text-description").attr("value", "");
		$("#input-select-mode").find("option[value='debug']").attr("selected", true);
		$("#input-select-gamehall").attr("value", "");
		$("#input-select-clientlobby").attr("value", "");
		$("#input-select-env").find("option[value='test']").attr("selected", true);
		$("#input-select-forceclean").find("option[value=0]").attr("selected", true);
		$("#input-select-pngquant").find("option[value=1]").attr("selected", true);
		$("#input-select-firkey").find("option[value='8d62e3c229fdce49dcc302bcd16ea15']").attr("selected", true);

		$("#input-select-mode").selectpicker("refresh");
		$("#input-select-env").selectpicker("refresh");
		$("#input-select-forceclean").selectpicker("refresh");
		$("#input-select-pngquant").selectpicker("refresh");
		$("#input-select-firkey").selectpicker("refresh");

		$("#add-build-config-modal-label").html("新增配置");
		$("#btn-add-build-config-confirm").show();
		$("#btn-edit-build-config-confirm").hide();
		$("#add-build-config-modal").modal();
	});

	$("#btn-add-build-config-confirm").click(function () {
		let formData = getFormData();
		requestSaveBuildConfig(formData);
	});

	$(".btn-edit-build-config").click(function (evt) {
		let target = evt.target;
		if (!target) {
			return ;
		}

		let id = target.id;
		let buildId = id.substring(id.lastIndexOf("-") + 1);
		editConfigId = buildId;

		$.ajax({
			url: "/build/getConfig",
			data: {
				id: buildId
			},
			success: function (res) {
				let data = JSON.parse(res);
				if (!data.success) {
					showTips(data.msg);
					return ;
				}

				let config = data.data;
				console.log(config);
				// 填充数据
				$("#input-text-config-name").attr("value", config.name);
				$("#input-text-version").attr("value", config.version);
				$("#input-text-description").attr("value", config.description);

 				$("#input-select-mode").selectpicker('val', config.params.mode);
 				$("#input-select-gamehall").selectpicker('val', config.params.gamehallbranch);
 				$("#input-select-clientlobby").selectpicker('val', config.params.lobbybranch);

				$("#input-select-env").find("option[value=" + config.params.env + "]").attr("selected", true);
				$("#input-select-forceclean").find("option[value=" + config.params.forceclean + "]").attr("selected", true);
				$("#input-select-pngquant").find("option[value=" + config.params.pngquant + "]").attr("selected", true);
				$("#input-select-firkey").find("option[value=" + config.params.fir + "]").attr("selected", true);

				$("#add-build-config-modal-label").html("编辑配置");
				$("#btn-add-build-config-confirm").hide();
				$("#btn-edit-build-config-confirm").show();
				$("#add-build-config-modal").modal();

				$("#input-select-mode").selectpicker("refresh");
				$("#input-select-env").selectpicker("refresh");
				$("#input-select-forceclean").selectpicker("refresh");
				$("#input-select-pngquant").selectpicker("refresh");
				$("#input-select-firkey").selectpicker("refresh");
			},
			fail: function (res) {
				showTips("查询记录失败");
			},
			error: function () {
				showTips("系统错误");
			}
		});
	});

	$("#btn-edit-build-config-confirm").click(function () {
		let id = editConfigId;
		let formData = getFormData();
		formData.isUpdate = true;
		formData.id = id;
		requestSaveBuildConfig(formData);
	});

	$(".btn-del-build-config").click(function (evt) {
		let target = evt.target;
		if (!target) {
			return ;
		}

		let id = target.id;
		let configId = id.substring(id.lastIndexOf("-") + 1);
		let name = $("#td-build-config-name-" + configId).html();

		showTips(`确认要删除<b>[${name}]</b>吗？`, null, "确定", function () {
			$.ajax({
				url: "/build/delConfig",
				data: {
					id: configId
				},
				success: function (res) {
					let data = JSON.parse(res);
					if (!data.success) {
						showTips(data.msg);
						return ;
					}

					showTips(data.msg, null, null, null, null, function () {
						window.location.reload();
					});
				}
			})
		}, "取消", function () {

		});

	});

	let addBuildTask = function (type, configId) {
		$.ajax({
			url: "/task/addTask",
			data: {
				configId: configId,
				type: type
			},
			success: function (res) {
				let data = JSON.parse(res);

				if (!data) {
					showTips(data.msg);
					return;
				}

				showTips(data.msg);
			},
			fail: function () {
				showTips("添加打包任务失败");
			}
		})

	};

	let btnBuildEvent = function (evt, type) {
		let target = evt.target;
		if (!target) {
			return ;
		}

		let id = target.id;
		let configId = id.substring(id.lastIndexOf("-") + 1);

		if (!configId || configId.length === 0) {
			return ;
		}

		addBuildTask(type, configId);
	};

	$(".btn-build-apk").click(function (evt) {
		btnBuildEvent(evt, "apk");
	});

	$(".btn-build-ios").click(function (evt) {
		btnBuildEvent(evt, "ios");
	});

	$(".btn-build-all").click(function (evt) {
		btnBuildEvent(evt, "all");
	});

	$(".btn-build-gamezip").click(function (evt) {
		btnBuildEvent(evt, "gamezip");
	});

	$(".btn-build-hallzip").click(function (evt) {
		btnBuildEvent(evt, "hallzip");
    });
    
    $("#btn-search").click(function () {
        let keyword = $("#input-text-keyword").val();
        if (isEmptyString(keyword)) {
            showTips("搜索关键字不能为空！");
            return;
        }

        $.ajax({
            url: "/build/allconfigs/",
            data: {
                keyword: keyword
            },
            success: function (res) {
                let data = JSON.parse(res);
                if (!data.success) {
                    showTips(data.msg);
                    return;
                }
            },
            fail: function () {
                showTips("执行失败！");
            },
            error: function () {
                showTips("系统错误！");
            }
        });
    });
});
$(function () {

	let createRootMenuItem = function (data, islink) {
		let html = "";
		if (islink) {
			html += "<li class='nav-item'>";
			html += "<a class='nav-link' href='" + data.link + "' target='main-web-frame'>";
			html += "<img class='nav-icon' src='" + data.icon + "' />" + data.text + "</a>";
			html +=	"</li>";
		} else {
			html = "<a class='nav-link' href='#navbarDropdown-" + data.mid + "' aria-controls='navbarDropdown' role='button' data-toggle='collapse' aria-haspopup='true' aria-expanded='true'>";
			html +=	"<img class='nav-icon' src='" + data.icon + "' />" + data.text + "</a>";
		}
		html += "";
		return html;
	};

	let createSubMenuItem = function (data) {
		let html = "";
		html += "<li class='nav-item'>";
		html += "<a href='" + data.link + "' class='nav-link' target='main-web-frame'>";
		html += "<img class='nav-icon' src='" + data.icon + "' />" + data.text + "</a>";
		html += "</li>";
		html += "";
		return html;
	};

	let requestUserMenus = function () {
		$.ajax({
			url: './menu/getUserMenu',
			data: {},
			dataType: "json",
			success: function (res) {
				console.log(res);
				if (!res.success) {
					showTips(res.msg);
					return ;
				}

				let menus = res.data;
				let html = "";
				for (key in menus) {
					let arr = menus[key];
					html += "<ul class='nav flex-column'>";
					if (arr.length === 1) {
						html += "<li class='nav-item'>";
						html += createRootMenuItem(el, true);
						html += "</li>";
					} else {
						arr.forEach(el => {
							if (el.isTitle) {
								html += "<li class='nav-item'>";
								html += createRootMenuItem(el, false);
								html += "<div class='collapse show' id='navbarDropdown-" + el.mid + "'>";
								html += "<ul class='' style='list-style:none;'>";
							} else {
								html += createSubMenuItem(el);
							}
						});
						html += "</ul></div>";
						html += "</li>";
					}
					html += "</ul>";
				};

				$("#wlgame-sidebar").html("").html(html);
			},
			fail: function () {
				showTips("查询菜单失败");
			},
			error: function () {
				showTips("系统错误");
			}
		});
	};

	requestUserMenus();

});
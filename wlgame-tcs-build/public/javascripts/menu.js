$(function () {

	let getMenuFormData = function () {
		let menuId = $("#input-menu-id").val();
		let menuName = $("#input-menu-name").val();
		let menuLink = $("#input-menu-link").val();
		let menuIcon = $("#input-menu-icon").val();
		let menuRoot = $("#input-menu-root").val();
		let menuEnabled = $("#input-menu-enabled").prop("checked");

		return {
			menuId: menuId,
			menuName: menuName,
			menuLink: menuLink,
			menuIcon: menuIcon,
			menuRoot: menuRoot,
			menuEnabled: menuEnabled
		};
	};

	let reqSaveMenuData = function (formData) {
		if (formData.menuName === "") {
			showTips("菜单名称不能为空");
			return;
		}
		if (formData.menuLink === "") {
			showTips("目标链接不能为空");
			return;
		}

		$.ajax({
			url: "./updateMenu",
			data: formData,
			success: function (res) {
				let data = JSON.parse(res);

				if (!data.success) {
					showTips(data.msg);
					return;
				}

				$("#add-menu-modal").modal('hide');

				showTips(data.msg, null, null, null, null, function () {
					window.location.reload();
				});
			},
			fail: function (res) {
				console.log("fail", res);
				showTips("保存失败");
			},
			error: function (res) {
				console.log("error", res);
				showTips("系统错误");
			}
		});
	};

	$("#btn-add-menu").click(function () {
		$("#add-menu-modal-label").html("新增菜单项");
		$("#btn-add-menu-confirm").show();
		$("#btn-edit-menu-confirm").hide();
		$("#input-menu-id").attr({"disabled": false, "value": ""});
		$("#input-menu-link").attr("value", "");
		$("#input-menu-icon").attr("value", "");
		$("#input-menu-name").attr("value", "");
		$("#input-menu-enabled").prop("checked", true);
		$("#input-menu-root").find("option").attr("selected", false);
		$("#input-menu-root").find("option[value=1]").attr("selected", true);
		$("#add-menu-modal").modal();
	});

    $("#btn-add-menu-confirm").click(function () {
        let formData = getMenuFormData();
        reqSaveMenuData(formData);
    });

	$(".btn-edit-menu").click(function (evt) {
		let target = evt.target;
		if (!target) {
			return ;
		}

		let id = target.id;
		let mid = id.substring(id.lastIndexOf("-") + 1);

		$.ajax({
			url: "./getMenu",
			data: {
				mid: mid
			},
			success: function (res) {
				let data = JSON.parse(res);

				if (!data.success) {
					showTips(data.msg);
					return;
				}
				$("#add-menu-modal-label").html("编辑菜单项");
				$("#btn-add-menu-confirm").hide();
				$("#btn-edit-menu-confirm").show();
				$("#input-menu-id").attr({"disabled": true, "value": data.data.mid});
				$("#input-menu-link").attr("value", data.data.link);
				$("#input-menu-icon").attr("value", data.data.icon);
				$("#input-menu-name").attr("value", data.data.text);
				$("#input-menu-enabled").prop("checked", data.data.enabled);
				$("#input-menu-root").find("option").attr("selected", false);
				$("#input-menu-root").find("option[value=" + data.data.root + "]").attr("selected", true);
				$("#add-menu-modal").modal();
			},
			fail: function (res) {
				console.log("fail", res);
				showTips("获取菜单信息失败");
			},
			error: function (res) {
				console.log("error", res);
				showTips("系统错误");
			}
		});
	});

	$("#btn-edit-menu-confirm").click(function () {
		$("#add-menu-modal").modal('hide');
		let formData = getMenuFormData();
		formData.isUpdate = true;
		reqSaveMenuData(formData);
	});


	$(".btn-del-menu").click(function (evt) {
		let target = evt.target;
		if (!target) {
			return ;
		}

		let id = target.id;
		let mid = id.substring(id.lastIndexOf("-") + 1);
		showTips("确认要删除菜单（" + mid + "）吗？", null, "确认", function () {
			$.ajax({
				url: "./delMenu",
				data: {
					mid: mid
				},
				success: function (res) {
					let data = JSON.parse(res);
					if (!data.success) {
						showTips(data.msg);
						return ;
					}

					showTips(data.msg, null, null, null, null, function () {
						window.location.reload();
					});
				},
				fail: function (res) {
					console.log(res);
					showTips("删除失败");
				},
				error: function (res) {
					showTips("系统错误");
				}
			})
		}, "取消", function () {

		});
	});
});
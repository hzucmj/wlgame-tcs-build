$(function () {

	let loadAllBranches = function () {
		$.ajax({
			url: "/branch/getAllBranch",
			data: {

			},
			success: function (res) {
				let data = JSON.parse(res);

				if (!data.success) {
					showTips(data.msg);
					return ;
				}

				let gameHallBranch = data.data.gameHallBranch || [];
				let clientLobbyBranch = data.data.clientLobbyBranch || [];

				for (let idx in gameHallBranch) {
					$("#input-select-gamehall").append("<option value='" + gameHallBranch[idx].branch + "'>" + gameHallBranch[idx].branch + "</option>");
				}

				for (let idx in clientLobbyBranch) {
					$("#input-select-clientlobby").append("<option value='" + clientLobbyBranch[idx].branch + "'>" + clientLobbyBranch[idx].branch + "</option>");
				}

				$("#input-select-gamehall").selectpicker("refresh");
				$("#input-select-clientlobby").selectpicker("refresh");
			},
			fail: function () {
				showTips("加载分支信息失败")
			},
			error: function () {
				showTips("系统错误")
			}
		})
	};
	// 加载所有分支
	loadAllBranches();

    // 开始打包
    $("#btn-start-build").click(function () {
        var platform = $("#input-select-platform").val();
        var mode = $("#input-select-mode").val();
        var gamehallbranch = $("#input-select-gamehall").val();
        var clientlobbybranch = $("#input-select-clientlobby").val();
        var env = $("#input-select-env").val();
        var forceclean = $("#input-select-forceclean").val();
        var pngquant = $("#input-select-pngquant").val();
        var email = $("#input-select-email").val();
        var emailtitle = $("#input-text-emailtitle").val();
        var fir_api_token = $("#input-select-firkey").val();

        var params = {
            platform: platform,
            mode: mode, 
            gamehallbranch: gamehallbranch,
            lobbybranch: clientlobbybranch,
            env: env,
            forceclean: parseInt(forceclean),
            email: email,
            subhead: emailtitle,
            fir_api_token: fir_api_token,
            pngquant: parseInt(pngquant)
        };

        console.log(JSON.stringify(params));

        var data = {
            params: JSON.stringify(params),
            owner: "leochen",
            type: 0
        };

        $.ajax({
            url: "../task/addTask",
            data: data,
            success: function (res) {
                console.log(res);
                var data = JSON.parse(res);
                if (!data.success) {
                    alert(data.msg);
                    return;
                }

                alert(data.msg);
            },
            fail: function (res) {
                console.log(res);
                alert("操作失败，请稍候重试!");
            },
            error: function (res) {
                console.log(res);
                alert("系统错误!");
            }
        });

    });
});
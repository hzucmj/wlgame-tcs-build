$(function (){
	// 新增用户
	$("#btn-add-menu").click(function () {
		var username = $("#input-menu-username").val();
        var passwd = $("#input-menu-passwd").val();
        var role = $("#input-menu-role").val();

        var data = {
        	name:username,
        	password:passwd,
        	role:role,
        	enabled:0,
        }

        if(username == ""){
        	showTips("请输入用户名");
        	return;
        }else if(passwd == ""){
			showTips("请输入密码");
			return;
        }

        $.ajax({
        	url: "../user/addUser",
            data: data,
            success: function (res) {
                console.log(res);
                var data = JSON.parse(res);
                if (!data.success) {
                    showTips(data.msg);
                    return;
                }

                $("#btn-add-cancel").click();
                showTips(data.msg, null, null, null, null, function () {
                	location.reload();
                });
            },
            fail: function (res) {
                console.log(res);
                showTips("操作失败，请稍候重试!");
            },
            error: function (res) {
                console.log(res);
                showTips("系统错误!");
            }
        });
	});

	// 修改用户资料
	$("#btn-edit-menu").click(function () { 
		var username = $("#input-edit-username").val();
        var passwd = $("#input-edit-passwd").val();
        var role = $("#input-edit-role").val();
        var useruid = $("#edit-user-modal-label").attr("name");

        if (passwd.length === 0) {
        	passwd = null;
        }

        var data = {
        	indexData:{uid:useruid},
        	data:{
	        	name:username,
	        	password:passwd,
	        	role:role,
	        	enabled:0,
        	},
        }

        if(username == ""){
        	showTips("请输入用户名");
        	return;
        }else if(passwd == ""){
			showTips("请输入密码");
			return;
        }

		$.ajax({
        	url: "../user/editUser",
            data: data,
            success: function (res) {
                console.log(res);
                var data = JSON.parse(res);
                if (!data.success) {
                    showTips(data.msg);
                    return;
                }

                $("#btn-edit-cancel").click();
                showTips(data.msg, null, null, null, null, function () {
                	location.reload();
                });
            },
            fail: function (res) {
                console.log(res);
                showTips("操作失败，请稍候重试!");
            },
            error: function (res) {
                console.log(res);
                showTips("系统错误!");
            }
        });
	});

	// 编辑用户
	$("button[name='userEdit']").click(function() {
		$("#edit-user-modal-label").attr("name", $(this).val());

        $("#input-edit-username").val("");
        $("#input-edit-passwd").val(""); // 不需要显示密码
        $("#input-edit-role").val(1);

		var data = {uid:$(this).val()};
		$.ajax({
        	url: "../user/searchUser",
            data: data,
            dataType: "json",
            success: function (res) {
                console.log(res);
                console.log($("#input-edit-role").val());
                $("#input-edit-username").val(res.data[0].name);
                // $("#input-edit-passwd").val(res.password); // 不需要显示密码
                $("#input-edit-role").val(res.data[0].role);
            },
            fail: function (res) {
                console.log(res);
            },
            error: function (res) {
                console.log(res);
            }
        });
	});

	// 删除用户
	$("button[name='userDelete']").click(function() {
		var data = {uid:$(this).val()};
		$.ajax({
        	url: "../user/deleteUser",
            data: data,
            success: function (res) {
                console.log(res);
                var data = JSON.parse(res);
                if (!data.success) {
                    showTips(data.msg);
                    return;
                }

                showTips(data.msg, null, null, null, null, function () {
                	location.reload();
                });
            },
            fail: function (res) {
                console.log(res);
                showTips("操作失败，请稍候重试!");
            },
            error: function (res) {
                console.log(res);
                showTips("系统错误!");
            }
        });
	});
});
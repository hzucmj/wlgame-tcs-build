
var TAG = "DB";
var mysql = require("mysql");
var DB = {};

module.exports = DB;

/**
 * 创建db连接池
 */
let __createPool = function (host, port, dbname, user, pwd) {
    var dbInfo = {
        host: host,
        port: port,
        database: dbname,
        user: user,
        password: pwd,
    };
    let dbPool = mysql.createPool(dbInfo);
    return dbPool;
};

DB.init = function (config) {
    config = {
        "host": "172.16.101.47",
        "port": "3306",
        "database": "db_tcs_build",
        "user": "root",
        "password": "root"
    };
    DB.dbPool = __createPool(config.host, config.port, config.database, config.user, config.password);
},

DB.executeSql = function (sql, callback) {
    console.log(TAG, "executeSql：", sql);
    DB.dbPool.getConnection(function (err, conn) {
        if (err) {
            console.log(err);
            callback(err, null, null);
        } else {
            conn.query(sql, function (err, values, fields) {
                //释放连接
                conn.release();
                //事件驱动回调
                callback(err, values, fields);
            });
        }
    });
};

DB.insertTableRecord = function (tableName, data, callback) {

    let sql = "INSERT INTO";

    sql = sql + " " + tableName + " (";

    for (let key in data) {
        sql = sql + key + ","
    }

    sql = sql.substring(0, sql.length - 1);

    sql = sql + ") VALUES (";

    for (let key in data) {
        let type = typeof (data[key]);
        let value = data[key];
        if (type === "string") {
            sql = sql + "'" + value + "',";
        } else if (type === "number") {
            sql = sql + value + ",";
        } else {
	        sql = sql + "'" + value + "',";
        }
    }

    sql = sql.substring(0, sql.length - 1);

    sql = sql + ");";

    console.log("insertTableRecord", sql);

    DB.executeSql(sql, callback);
};

DB.updateTableRecord = function (tableName, indexData, data, callback) {
    let sql = "UPDATE " + tableName + " SET ";

    for (let key in data) {
        let type = typeof (data[key]);
        let value = data[key];
        if (type === "string") {
            sql = sql + key + "='" + value + "',";
        } else if (type === "number") {
            sql = sql + key + "=" + value + ",";
        }
    }

    sql = sql.substring(0, sql.length - 1);

    sql = sql + " WHERE ";

    for (let key in indexData) {
        let type = typeof (indexData[key]);
        let value = indexData[key];
        if (type === "string") {
            sql = sql + key + "='" + value + "' AND ";
        } else if (type === "number") {
            sql = sql + key + "=" + value + " AND ";
        }
    }

    sql = sql.substring(0, sql.length - 5);

    sql = sql + ";";

    console.log("updateTableRecord", sql);

    DB.executeSql(sql, callback);
}
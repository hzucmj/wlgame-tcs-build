#!/usr/bin/python
# -*- coding: UTF-8 -*-

import pymysql
import config

class Db():

    def getConn(self):
        conn = pymysql.connect(host=config.MYSQL_HOST, port=config.MYSQL_PORT, user=config.MYSQL_USER, passwd=config.MYSQL_PASSWORD, db=config.MYSQL_DATABASE)
        return conn

    def executeSql(self, sql):
        conn = self.getConn()
        cursor = conn.cursor()
        result = None
        try:
            cursor.execute(sql)
            result = cursor.fetchall()
        except:
            print("Error1")
        finally:
            conn.close()

        return result
        
    def updateRecord(self, sql):
        conn = self.getConn()
        try:
            cursor = conn.cursor()
            cursor.execute(sql, ())
            conn.commit()
        except:
            print("Error2")
        finally:
            conn.close()
        return

db = Db()
executeSql = db.executeSql
updateRecord = db.updateRecord
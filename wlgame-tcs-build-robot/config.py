#!/usr/bin/python
# -*- coding: UTF-8 -*-
IS_DEBUG = False

WORK_ROOT = "H:\\testapk\\autobuildtools" if IS_DEBUG else "/Users/learn/Documents/tcsbuildquick-bryan/autobuildtools"

MYSQL_HOST = "172.16.101.47" if IS_DEBUG else "127.0.0.1"
MYSQL_PORT = 3306
MYSQL_DATABASE = "db_tcs_build"
MYSQL_USER = "root"
MYSQL_PASSWORD = "root"

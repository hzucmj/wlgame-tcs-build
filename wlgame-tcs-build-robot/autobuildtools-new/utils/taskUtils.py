#!/usr/bin/python
# -*- coding: UTF-8 -*-

import pymysql
import config

class Db():

    def getConn(self):
        conn = pymysql.connect(host=config.MYSQL_HOST, port=config.MYSQL_PORT, user=config.MYSQL_USER, passwd=config.MYSQL_PASSWORD, db=config.MYSQL_DATABASE)
        return conn

    def executeSql(self, sql):
        conn = self.getConn()
        cursor = conn.cursor()
        result = None
        try:
            cursor.execute(sql)
            result = cursor.fetchall()
        except:
            print("Error1")
        finally:
            conn.close()

        return result
        
    def updateRecord(self, sql):
        conn = self.getConn()
        try:
            cursor = conn.cursor()
            cursor.execute(sql, ())
            conn.commit()
        except:
            print("Error2")
        finally:
            conn.close()
        return

# 未开始
TASK_STATUS_NONE = 0
TASK_STATUS_RUNNING = 1
TASK_STATUS_SUCCESSED = 2
TASK_STATUS_FAILED = 3
TASK_STATUS_FAILED_ERROR_ARGUMENTS = 4
TASK_STATUS_SUCCESSED_EXCEPT_FTP = 5
TASK_STATUS_SUCCESSED_EXCEPT_FIR = 6
TASK_STATUS_SUCCESSED_EXCEPT_MAIL = 7

def updateTaskStatus(taskid, status):

	sql = "UPDATE tb_task set status=%d WHERE taskid='%s'" % (status, taskid)

	try:
		db = Db()
		db.updateRecord(sql)
	except:
		print("taskUtils.py", "updateTaskStatus Error.")

def updateTask(taskid, params):

	sql = "UPDATE tb_task set"
	for key in params:
		sql = "%s %s='%s'" % (sql, key, params[key])

	sql = "%s WHERE taskid='%s'" % (sql, taskid)

	print("taskUtils.py", sql)
	try:
		db = Db()
		db.updateRecord(sql)
	except:
		print("taskUtils.py", "UpdateTask Error.")
